<?php

return [
    'remove_success' => 'Remove success!',
    'restore_success' => 'Restore success!',
    'limit_backup' => 'Backup limited!',
    'backup_success' => 'Backup success!',
    'Data not found' => 'Data not found',
    'Update Successfully' => 'Update Successfully',
    'Password Update successfully' => 'Password Update successfully',
    'Old Password is Wrong' => 'Old Password is Wrong',
];