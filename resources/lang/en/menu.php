<?php

return [

    'dashboard' => 'Dashboard',
    'product' => 'Product',
    'add_csv_file' => 'Add CSV file',
    'product_list' => 'Product List',
    'news' => 'News',
    'articles' => 'Articles',
    'category' => 'Category',
    'user_submit' => 'User Submit',
    'detail_list' => 'Detail List',
    'setting' => 'Setting',
    'profile_setting' => 'Profile Setting',
    'users' => 'Users',
    'backup_restore' => 'Backup/Restore',
    'backup' => 'Backup',
    'profile' => 'Profile',
    'log_out' => 'LOG OUT',
    'Upload Csv' => 'Upload Csv',
    'Create Category' => 'Create Category',
    'Edit News' => 'Edit News',
    'trash_can' =>'Trash Can',
];