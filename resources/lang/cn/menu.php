<?php

return [

    'dashboard' => '仪表板',
    'product' => '产品',
    'add_csv_file' => '添加CSV文件',
    'product_list' => '产品列表',
    'news' => '新闻',
    'articles' => '文章',
    'category' => '类别',
    'user_submit' => '用户提交',
    'detail_list' => '详细清单',
    'setting' => '设置',
    'profile_setting' => '配置文件设置',
    'users' => '用户数',
    'backup_restore' => '备份/还原',
    'backup' => '后备',
    'profile' => '轮廓',
    'log_out' => '登出',
    'Upload Csv' => '上传CSV',
    'Create Category' => '创建类别',
    'Edit News' => '编辑新闻',
    'trash_can' =>'垃圾箱',
];