<?php

return [
    'remove_success' => '移除成功!',
    'restore_success' => '恢复成功!',
    'limit_backup' => '备份受限!',
    'backup_success' => '备份成功!',
    'Data not found' => '找不到数据',
    'Update Successfully' => '更新成功',
    'Password Update successfully' => '密码更新成功',
    'Old Password is Wrong' => '旧密码错误',
];