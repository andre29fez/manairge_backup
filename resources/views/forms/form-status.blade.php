<div id="selecsts" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">{{__('form.Select Status')}}</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <select id="sts" onchange="selse()" class="form-control">
            <option value="">--Status--</option>
                <option value="Shipping">Shipping</option>
                <option value="Arrival">Arrival</option>
                <option value="Collected">Collected</option>
             </select>       
            </div>
            <div class="modal-footer">
                <div class="btn-group" role="group" aria-label="Button group">
                    <button class="btn btn-primary" id="update">{{__('form.action.Change')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>