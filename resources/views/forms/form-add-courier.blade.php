<div id="mcourier" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">{{__('form.Add Courier Code')}}</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form id="fAddcourier" class="form-horizontal well" action="" method="POST">
@csrf
<input id="is_restore" type="text" name="is_restore" style="display:none;"/>
<input id="is_update" type="text" name="is_update" style="display:none;"/>
            <div class="form-group row {!! $errors->has('courier_code') ? 'has-error' : '' !!}">
                <label for="courier_code" class="control-label col-lg-4">{{__('form.courier_code')}}</label>
                <div class="col-lg-8">
                    <input class=" form-control" id="courier_code" name="courier_code" type="text" value="{!! old('courier_code') !!}">
                    {!! $errors->first('courier_code', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group row {!! $errors->has('container_code') ? 'has-error' : '' !!}">
                <label for="container_code" class="control-label col-lg-4">{{__('form.container_code')}}</label>
                <div class="col-lg-8">
                    <input class=" form-control" id="container_code" name="container_code" type="text" value="{!! old('container_code') !!}">
                    {!! $errors->first('container_code', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group row {!! $errors->has('size') ? 'has-error' : '' !!}">
            <label for="size" class="control-label col-lg-4">{{__('form.size')}}</label>
            <div class="col-lg-8">
                <input class=" form-control" id="size" name="size" type="number" min="0.00000" max="999.99" value="{!! old('size') !!}">
                {!! $errors->first('size', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
            <div class="form-group row {!! $errors->has('weight') ? 'has-error' : '' !!}">
                <label for="weight" class="control-label col-lg-4">{{__('form.weight')}}</label>
                <div class="col-lg-8">
                    <input class=" form-control" id="weight" name="weight" type="text" value="{!! old('weight') !!}">
                    {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            </form>            
            </div>
            <div class="modal-footer">
                <div class="btn-group" role="group" aria-label="Button group">
                    <button id="addcourier" class="btn btn-primary">{{__('form.action.Submit')}}</button>
                </div>
            </div>
        </div>
    </div>
    
</div>