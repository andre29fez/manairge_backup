@extends('layout.master')
@section('css')
@endsection
@section('content')
<section id="main-content">
          <section class="wrapper">
<!-- page start-->
<div class="row">
<div class="col-sm-12 overflow-auto">
<section class="card">

<header class="card-header">
    {{ $menu }}
<span class="tools pull-right">
<a href="javascript:;" class="fa fa-chevron-down"></a>
<a href="javascript:;" class="fa fa-times"></a>
</span>
</header>
<!-- card body -->
<div class="card-body">
</div>
<!-- end card body -->
<!-- card table -->
<div class="card-body overflow-auto">
    <div class="adv-table">
    <!-- BUTTON CONTROL -->
    <div class="clearfix">
    </div>
    <div class="space15"></div>
    <!-- END BUTTON CONTROL -->
    <!-- DISINI TABLE -->
    </div>
</div>
<!-- end card table -->
</section>
</div>
</div>
<!-- page end-->
    </section>
</section>
<!--main content end-->
@endsection
@section('js')
@endsection