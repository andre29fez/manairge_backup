@if(isset($category))
<form action="{{ route('category.update',[$category->id]) }}" method="POST" enctype="multipart/form-data" role="form">
                @csrf
                @method('PUT')
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">{{__('form.Category Name')}}</label>
                    <input id="name" class="form-control" type="text" name="name" value="{{$category->name}}">
                </div>
                <div class="form-group">
                    <label for="image">{{__('form.Image')}}</label>
                    <div style="width:50%;margin:0 auto;padding:10px;">
                    <img class="img-thumbnail" src="{{ asset('img/news/'.$category->image) }}" alt="{{ $category->name }}">
                    </div>
                    <input id="image" class="form-control-file" type="file" name="image">
                </div>
               
            </div>
            <div class="modal-footer">
                <div class="btn-group" role="group" aria-label="Button group">
                    <button class="btn btn-primary" type="submit">{{__('form.action.Save')}}</button>
                </div>
            </div>
            </form>
@endif            