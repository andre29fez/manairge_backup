@extends('layout.master')
@section('css')
@endsection
@section('content')
<section id="main-content">
          <section class="wrapper">
<!-- page start-->
<div class="row">
<div class="col-sm-12 overflow-auto">
<section class="card">

<header class="card-header">
    {{ __('menu.category') }}
<span class="tools pull-right">
<button type="button" class="btn btn-danger" onclick="openCat()" > <i class="fa fa-pencil"></i>&nbsp;{{ __('table.action.create') }}</button>

@include('forms.form-modal', ['title'=>__('form.Edit Category')])
<a href="javascript:;" class="fa fa-chevron-down"></a>
<a href="javascript:;" class="fa fa-times"></a>
</span>
<div id="addcategory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">{{__('form.Add Category')}}</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data" role="form">
                @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">{{__('form.Category Name')}}</label>
                    <input id="name" class="form-control" type="text" name="name">
                </div>
                <div class="form-group">
                    <label for="image">{{__('form.Image')}}</label>
                    <input id="image" class="form-control-file" type="file" name="image">
                </div>
               
            </div>
            <div class="modal-footer">
          
                <div class="btn-group" role="group" aria-label="Button group">
                    <button class="btn btn-primary" type="submit">{{__('form.action.Save')}}</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</header>
<!-- card body -->
<div class="card-body">
</div>
<!-- end card body -->
<!-- card table -->
<div class="card-body overflow-auto">
    <div class="adv-table">
    <table class="display table table-bordered table-striped" style="width:100%" id="tcat">
                            <thead>
                                <tr>
                                    <th>{{ __('table.ID') }}</th>
                                    <th>{{ __('table.Image') }}</th>
                                    <th>{{ __('table.name') }}</th>
                                    <th>{{ __('table.Slug') }}</th>
                                    <th>{{ __('table.Status') }}</th>
                                    <th>{{ __('table.action.action') }}</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($categories as $category)
                                <tr class="gradeX">
                                    <td>{{ $category->id }}</td>
                                    <td>
                                        <img src="{{ asset('img/news/'.$category->image) }}" alt="{{ $category->name }}" width="40px">
                                    </td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td>{{ $category->status ? 'Active' : 'Inactive' }}</td>
                                    <td>
                                        <div class="btn-group-horisontal text-center">
                                        <a class="btn btn-warning btn-flat load-form-modal" data-url="{{ route('category.edit',[$category->id]) }}" data-toggle ="modal" data-target='#form-modal' href="#"><i class="fa fa-edit"></i></a>
                                            
                                            <a href="javascript:void(0)" class="btn btn-danger btn-flat"
                                                onclick="event.preventDefault();
                                                        document.getElementById('category-delete-form-{{$category->id}}').submit();">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            <form id="category-delete-form-{{$category->id}}" action="{{ route('category.destroy',$category->id) }}" method="POST" style="display: none;">
                                                @csrf 
                                                @method('DELETE')
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                            <tfoot>
                                <tr>
                                <th>{{ __('table.ID') }}</th>
                                    <th>{{ __('table.Image') }}</th>
                                    <th>{{ __('table.name') }}</th>
                                    <th>{{ __('table.Slug') }}</th>
                                    <th>{{ __('table.Status') }}</th>
                                    <th>{{ __('table.action.action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
    </div>
</div>
<!-- end card table -->
</section>
</div>
</div>
<!-- page end-->
    </section>
</section>
<!--main content end-->
@endsection
@section('js')
<script>
 var oUsers = $('#tcat').dataTable({
        "order": [[0,"asc"]],
        "columnDefs": [{
        "targets"  : 'no-sort',
        "orderable": false,
        },
        { "width": "5%", "targets": 0 }],
        "pageLength": 25,
    });
function openCat(){
    $('#addcategory').modal('show');
}
var Modal = {
    init: function () {
        this.initEditModal();
        
    },
    initEditModal: function() {
        $(document).on('click', '.load-form-modal', function(event){
            console.log('Modal: '+ $(this).attr('data-url'));
            $('#form-modal .isi').load($(this).attr('data-url'));
            event.preventDefault();
        });
    },
};

Modal.init();
</script>
@endsection