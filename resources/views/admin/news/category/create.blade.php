@extends('layout.master')
@section('css')
@endsection
@section('content')
<section id="main-content">
          <section class="wrapper">
<!-- page start-->
<div class="row">
<div class="col-sm-12 overflow-auto">
<section class="card">

<header class="card-header">
{{ __('menu.Create Category') }}
<span class="tools pull-right">
<a href="javascript:;" class="fa fa-chevron-down"></a>
<a href="javascript:;" class="fa fa-times"></a>
</span>
</header>
<!-- card body -->
<div class="card-body">
</div>
<!-- end card body -->
<!-- card table -->
<div class="card-body overflow-auto">
<form action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data" role="form">
    @csrf
<div class="row">
    <div class="col-md-6">
        <div class="box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Create Category</h3>
            </div>

            <div class="box-body">
                <div class="form-group">
                    <label for="categoryname">Category Name</label>
                    <input type="text" name="name" class="form-control" id="categoryname">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Category Image</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <input type="file" name="image" id="categoryimage">
                    <p class="help-block">(Image must be in .png or .jpg format)</p>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="status"> Active
                    </label>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Submit</button>
            </div>
        </div>
    </div>
</div>
</form>
</div>
<!-- end card table -->
</section>
</div>
</div>
<!-- page end-->
    </section>
</section>
<!--main content end-->
@endsection
@section('css')
@endsection