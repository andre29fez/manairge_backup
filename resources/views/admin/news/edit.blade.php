@extends('layout.master')
@section('css')
<!-- include codemirror (codemirror.css, codemirror.js, xml.js, formatting.js) -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
<link rel="stylesheet" href="{{ asset('assets/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/summernote/summernote-bs4.css') }}">
@endsection
@section('content')
<section id="main-content">
          <section class="wrapper">
<!-- page start-->
<div class="row">
<div class="col-sm-12 overflow-auto">
<section class="card">

<header class="card-header">
    {{ $menu }}
<span class="tools pull-right">
<a href="javascript:;" class="fa fa-chevron-down"></a>
<a href="javascript:;" class="fa fa-times"></a>
</span>
</header>
<!-- card body -->
<div class="card-body">
</div>
<!-- end card body -->
<!-- card table -->
<div class="card-body overflow-auto">
       
            <form action="{{ route('news.update',[$news->id]) }}" method="POST" enctype="multipart/form-data" role="form">
                @csrf
                @method('PUT')
                <div class="row">
                <div class="col-md-8">
                    <div class="box-primary">
                        <div class="box-body">
                            <div class="form-group {!! $errors->has('title') ? 'error' : '' !!}">
                                <label for="newstitle">{{__('form.Title')}} *</label>
                                <input type="text" name="title" class="form-control" id="newstitle" value="{{$news->title}}">
                                {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('details') ? 'error' : '' !!}">
                                <label>{{__('form.News Details')}} *</label>
                                <textarea class="textarea summernote" name="details" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                {!! $news->details !!}
                                </textarea>
                                {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box-primary">
                        <div class="box-body">
                            <div class="form-group">
                                <label>{{__('form.Categories')}}</label>
                                <select name="category_id" class="form-control select2" id="#inputID" style="width: 100%;">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ ($news->id == $category->id) ? 'selected':''}}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group {!! $errors->has('image') ? 'error' : '' !!}">
                                <label for="newsimage">{{__('form.Featured Image')}}</label>
                                @if($news->image != '')
                                <div style="width:50%;margin:0 auto;padding:10px;">
                                <img class="img-thumbnail" src="{{ asset('img/news/'.$news->image) }}" alt="image">
                                </div>
                                @endif
                                <input class="form-control"type="file" name="image" id="newsimage">
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
        
                            </div>
                            <div class="form-group {!! $errors->has('video') ? 'error' : '' !!}">
                                <label for="newsimage">{{__('form.Featured Video')}}</label>
                                @if($news->video != '' || $news->video != null)
                                <div style="width:50%;margin:0 auto;padding:10px;">
                                <img class="img-thumbnail" src="{{ asset('img/news/vthumbs/'.$news->video) }}" alt="video">
                                </div>
                                @endif
                                <input class="form-control"type="file" name="video" id="video">
                                {!! $errors->first('video', '<p class="help-block">:message</p>') !!}
        
                            </div>
                            {{--<div class="checkbox">
                                <label>
                                    <input type="checkbox" name="status"> Published
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="featured"> Featured
                                </label>
                            </div>--}}
                        </div>
                        <hr>
                        <div class="box-footer pull-right">
                            <button type="submit" class="btn btn-primary btn-flat">{{__('form.action.Update')}}</button>
                        </div>
                    </div>
                </div>
                </div>
            </form>

</div>
<!-- end card table -->
</section>
</div>
</div>
<!-- page end-->
    </section>
</section>
<!--main content end-->
@endsection
@section('js')
<!-- iCheck -->
<script src="{{ asset('assets/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/select2/js/select2.full.min.js') }}"></script>
    <script>
        $(function () {

            $('.select2').select2();
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue'
            });
        });
    </script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>
<script src="{{ asset('assets/summernote/summernote-bs4.js') }}"></script>
<script>
$('.summernote').summernote({
  height: 400,   //set editable area's height
  codemirror: { // codemirror options
    theme: 'monokai'
  }
});
</script>
@endsection