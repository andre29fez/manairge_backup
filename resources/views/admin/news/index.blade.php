@extends('layout.master')
@section('css')
@endsection
@section('content')
<section id="main-content">
          <section class="wrapper">
<!-- page start-->
<div class="row">
<div class="col-sm-12 overflow-auto">
<section class="card">

<header class="card-header">
    {{ $menu }}
<span class="tools pull-right">
<a  title="add"  href="{{ route('news.create') }}"><button type="button" class="btn btn-danger"> <i class="fa fa-pencil"></i>&nbsp;{{ __('table.action.create') }}</button></a>
<a href="javascript:;" class="fa fa-chevron-down"></a>
<a href="javascript:;" class="fa fa-times"></a>
</span>
</header>
<!-- card body -->
<div class="card-body">
</div>
<!-- end card body -->
<!-- card table -->
<div class="card-body overflow-auto">

    <div class="adv-table">
        <!-- Start table -->
        <!-- id="category-table" -->
        <table class="display table table-bordered table-striped" id="dynamic-table"> 
                            <thead>
                                <tr>
                                <th>{{ __('table.ID') }}</th>
                                    <th>{{ __('table.Title') }}</th>
                                    <th>{{ __('table.Category') }}</th>
                                    <th>{{ __('table.Image') }}</th>
                                    <th>{{ __('table.Video') }}</th>
                                    <th>{{ __('table.action.action') }}</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($newslist as $news)
                                <tr class="gradeX">
                                    <td>{{ $news->id }}</td>
                                    <td>{{ $news->title }}</td>
                                    <td>{{ @$news->category->name }}</td>
                                    <!-- <td>{{ str_limit(strip_tags($news->details),300) }}</td> -->
                                    <td class="text-center">
                                        <img src="{{ asset('img/news/'.$news->image) }}" alt="{{ $news->image }}" height="60px">
                                    </td>
                                    <!-- <td>{{ $news->featured ? 'Featured' : '' }}</td> -->
                                    <td class="text-center">
                                        <img src="{{ asset('img/news/vthumbs/'.$news->video) }}" alt="{{ $news->video }}" height="60px">
                                    </td>
                                    <td>
                                        <div class="btn-group-horisontal text-center">
                                            <!-- <a href="" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></a> -->
                                            <a href="{{ route('news.edit',$news->id) }}" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-danger btn-flat"
                                                onclick="event.preventDefault();
                                                    document.getElementById('news-delete-form-{{$news->id}}').submit();">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            <form id="news-delete-form-{{$news->id}}" action="{{ route('news.destroy',$news->id) }}" method="POST" style="display: none;">
                                                @csrf 
                                                @method('DELETE')
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                            <tfoot>
                                <tr>
                                <th>{{ __('table.ID') }}</th>
                                    <th>{{ __('table.Title') }}</th>
                                    <th>{{ __('table.Category') }}</th>
                                    <th>{{ __('table.Image') }}</th>
                                    <th>{{ __('table.Video') }}</th>
                                    <th>{{ __('table.action.action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
        <!-- End table -->
    </div>
</div>
<!-- end card table -->
</section>
</div>
</div>

<!-- page end-->
    </section>
</section>
<!--main content end-->
@endsection
@section('css')
@endsection