@extends('layout.master')
@section('css')
<style>
.profile-nav ul{
    margin-top:0px;
}
.list {
  font-family:sans-serif;
  margin:0;
  display:none;
  position:absolute;
  width:315px;
}
.list span,
.list a {
  display: inline-block;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  width:100%;
  text-decoration: none;
}
.list > li {
  display:block;
  background-color: #eee;
  box-shadow: inset 0 1px 0 #fff;
}
.profile-nav ul > li{
    line-height:38px;
    background:#fff;
}
.profile-nav ul > li > a{
    border-left:0;
    padding: 0px 10px;
}
.profile-nav ul > li > a:hover{
    border-left:0;
}
.avatar {
  max-width: 150px;
}
img {
  max-width: 100%;
}
h3 {
  font-size: 16px;
  margin:0 0 0.3rem;
  font-weight: normal;
  font-weight:bold;
}
p {
  margin:0;
}

input {
  border:solid 1px #ccc;
  border-radius: 5px;
  padding:7px 14px;
  margin-bottom:10px
}
input:focus {
  outline:none;
  border-color:#aaa;
}
.no-result {
  display:none;
}
.lev{
    width: 315px;
    background: url("../img/search-icon.png") no-repeat 10px 10px;
    background-color: rgba(0, 0, 0, 0);
    background-color: white;
    border: 1px solid #b5d6ea;
}
.lev:focus{
    width: 315px;
}
</style>
@endsection
@section('content')
@include('layout.notifications')
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <aside class="profile-nav col-lg-3">
                      <section class="panel">
                      @if(Auth::user()->authorizeRoles('admin'))
                      <div id="users">
                        <input type="text" class="form-control search lev" placeholder="{{ __('form.Search Name or Email') }}">
                        <ul class="list">
                        @foreach($users as $u)
                            <li>
                                <a href='{{ url("ProfileSetting/$u->id") }}'>
                                    <div class="name">{{ $u->name }} ( {{ $u->email }} )</div>
                                </a>
                            </li>
                        @endforeach    
                        </ul>

                        </div>
                        @endif
                          <div class="user-heading round">
                              <a href="#">
                                  <img src='{{url("$userData->profile_pic")}}' alt="">
                              </a>
                              <h1 style="margin-top:20px;">{{ ucfirst($userData->name) }}</h1>
                              <p>{{$userData->email}}</p>
                              <p style="margin-top:20px;">@foreach(Auth::user()->roles as $role)
                                        <button class="btn btn-sm btn-success">{{$role->name}}</button>
                                      @endforeach  
                              </p>
                          </div>

                          <ul class="nav nav-pills nav-stacked">
                              <!-- <li  class="active"><a href="profile-edit.html"> <i class="fa fa-edit"></i> Edit profile</a></li> -->
                          </ul>

                      </section>
                  </aside>
                  <aside class="profile-info col-lg-6">
                      <section class="panel">
                          
                          <div class="panel-body bio-graph-info" style="background:#fff;">
                              <h1 style="padding:10px;"> {{ __('form.Profile Info') }}</h1>
                              <form class="form-horizontal" action='{{ url("updateprofile/$userData->id") }}' method="post" role="form" enctype="multipart/form-data">
                                  {!! csrf_field() !!}
                                  <div class="form-group">
                                      <label  class="col-lg-6 control-label"> {{ __('form.Name')}}</label>
                                      <div class="col-lg-12">
                                          <input type="text" class="form-control" name="name" id="f-name" value="{{$userData->name}}" placeholder=" ">
                                      </div>
                                  </div>
                                 
                                 <div class="form-group">
                                      <label  class="col-lg-6 control-label">{{ __('form.Client Code')}}</label>
                                      <div class="col-lg-12">
                                          <input type="text" class="form-control" value="{{$userData->client_code}}" id="client_code" name="client_code" placeholder=" ">
                                      </div>
                                  </div>
                               
                                
                                  <div class="form-group">
                                      <label  class="col-lg-6 control-label">{{ __('form.Email')}}</label>
                                      <div class="col-lg-12">
                                          <input type="text" class="form-control" value="{{$userData->email}}" id="email" name="email" placeholder=" ">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label  class="col-lg-6 control-label">{{ __('form.Phone')}}</label>
                                      <div class="col-lg-12">
                                          <input type="text" class="form-control" value="{{$userData->phone}}" id="mobile" name="mobile" placeholder=" ">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label  class="col-lg-6 control-label">{{ __('form.Remarks')}}</label>
                                      <div class="col-lg-12">
                                        <textarea  class="form-control" name="remarks" >{{$userData->remark}}</textarea>
                                          
                                      </div>
                                  </div>

                                    <div class="form-group">
                                          <label  class="col-lg-6 control-label">{{ __('form.Change Avatar')}}</label>
                                          <div class="col-lg-12">
                                              <input type="file" class="file-pos" name="picture" id="exampleInputFile">
                                          </div>
                                      </div>

                                  <div class="form-group">
                                      <div class="col-lg-offset-6 col-lg-12" style="padding-bottom: 20px;text-align: right;">
                                            <button type="button" class="btn btn-default">{{ __('form.action.Cancel')}}</button>
                                          <button type="submit" class="btn btn-success">{{ __('form.action.Save')}}</button>
                                          
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </section>
                     
                  </aside>
                  <aside class="profile-info col-lg-3"> 
                  <section>
                          <div class="panel panel-primary" style="background:#fff;">
                              <div class="panel-heading" style="font-size: 20px;font-weight: 300;margin: 0 0 20px;padding:10px 0 6px 10px"> 
                              {{ __('form.Sets New Password')}}</div>
                              <div class="panel-body">
                                  <form class="form-horizontal" action='{{ url("changepassword/$userData->id") }}' method="post" role="form" enctype="multipart/form-data">
                                  {!! csrf_field() !!}
                                      
                                      <div class="form-group">
                                          <label  class="col-lg-6 control-label">{{ __('form.Old Password')}}</label>
                                          <div class="col-lg-12">
                                              <input type="password" class="form-control" name="old_pass" id="n-pwd" placeholder=" ">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label  class="col-lg-6 control-label"> {{ __('form.New Password')}}</label>
                                          <div class="col-lg-12">
                                              <input type="password" class="form-control" name="new_pass" id="rt-pwd" placeholder=" ">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-lg-offset-6 col-lg-12" style="padding-bottom: 20px;text-align: right;">
                                              <button type="button" class="btn btn-default">{{ __('form.action.Cancel')}}</button>
                                              <button type="submit" class="btn btn-info">{{ __('form.action.Save')}}</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </section>
                  </aside>
              </div>

              <!-- page end-->
          </section>
      </section>
    @endsection
    @section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
    <script>
    var options = {
        valueNames: [ 'name', 'email' ]
        };
        var userList = new List('users', options);
        userList.on('updated', function(list) {
        if (list.matchingItems.length == list.items.length) {
            $('.list').hide()
        } else {
            $('.list').show()
        }
        })
    </script>
    @endsection
