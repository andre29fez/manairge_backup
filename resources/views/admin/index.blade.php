<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="IsmiZhu">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, PintarApp, Pintarmedia, login page">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Scanner App</title>

    <!-- Bootstrap core CSS -->
     <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
     <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-reset.css') }}">
     <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
     <link rel="stylesheet" href="{{ URL::asset('css/style-responsive.css') }}">
     <link rel="stylesheet" href="{{ URL::asset('assets/toastr-master/toastr.css') }}">
     <style>
     .login-body{
        background:#e7ffc5;
     }
     .lock-wrapper{
      background:#ffffff;
      border-radius: 10px;
     }
     .lock-box{
       margin-top:120px;
     }
     .lock-wrapper img{
       height:75px;
       padding:6px;
       background:#ffffff;
     }
     .lock-wrapper h1{
        color:#698f3e;
        font-weight:800;
     }
     input[type="text"], input[type="password"] {
        margin-bottom: 20px;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        border: 1px solid #eaeaea;
        box-shadow: none;
        font-size: 12px;
     }
     .lock-wrapper .locked{
      color:#698f3e;
      margin-bottom:40px;
     }
     .btn-login{
       background:#698f3e;
       color:#fff;
        font-weight: 600;
        padding: 6px 70px;
        font-size: 16px;
        display:inline-block;
     }

     .fixed-bottom{
       color:#212121;
       
     }
     .fixed-bottom h2, .fixed-bottom h3{
       font-size: 23px;
       font-weight:600;
     }
     .fixed-bottom h3{
       margin-bottom:15px;
     }
     .fixed-bottom p{
      font-size: 17px;
      margin-bottom: 0px;
     }
     </style>
  
</head>
  <body class="login-body">
    <div class="container">
  @if (count($errors) > 0)
     
          @foreach ($errors->all() as $error)
          <div class="alert alert-info alert-block">
  <button type="button" class="close" data-dismiss="alert">×</button> 
  <strong>{{ $error }}</strong>
</div>
           
          @endforeach
      
    @endif
    <div class="lock-wrapper">
        <div class="shadow-sm lock-box text-center">
            <img src="{{ URL::asset('img/ic_logo.png') }}" alt="lock avatar">
            <h1>SIGN IN</h1>
            <!-- <span class="locked">Locked</span> -->
            <form role="form" action="{{ route('login') }}" method="post">
            {!! csrf_field() !!}
                <div class="login-wrap">
                  <input type="text" class="form-control" name="email" placeholder="Email address" autofocus>
                  <input type="password" class="form-control" name="password" placeholder="Password">
                  <span class="locked">Forgot password?</span>       
                  <button class="btn btn-lg btn-login" type="submit">Sign in</button>
                </div>
            </form>
        </div>
  </div>
  <div class="fixed-bottom text-center">
  <h2>巅峰贸易</h2>
  <h3>LC PINNACLE TRADING</h3>
  <p>LOT 1840, GROUND FLOOR, EASTWOOD INDUSTRIAL ESTATE, JALAN MIRI-BYPASS, 98000 MIRI, SARAWAK, MALAYSIA</p>
  <p>TEL: + 6013-836 8338 / + 6016-853 3399  |  Email: buzhidao9999@hotmail.com</p>      
  
  </div>
    </div>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('js/jquery.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ URL::asset('assets/toastr-master/toastr.js') }}"></script>
    <script>
       toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000; 
    toastr.options.timeOut = 0;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;
    @if(session("message"))
        toastr.info("{{ session("message") }}");
    @endif 
    </script>
  </body>
</html>
