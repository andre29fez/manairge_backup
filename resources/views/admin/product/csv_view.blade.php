@extends('layout.master')
@section('css')
@endsection
@section('content')
@include('layout.notifications')
<section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="card">
                          <header class="card-header">
                              {{ __('menu.Upload Csv')}}
                          </header>
                          <div class="card-body">
                              <form role="form" class="form-horizontal tasi-form" action='{{ url("admin/uploadcsv") }}' method="post" enctype="multipart/form-data">
                                 {!! csrf_field() !!}
                                  <div class="form-group has-success row">
                                      <label class="col-lg-2 control-label">{{ __('form.CSV File')}}</label>
                                      <div class="col-lg-10">
                                          <input type="file" class="form-control is-valid" id="validationServer01" placeholder="First name" name="file" required="" required="">
                                         <!--  <div class="valid-feedback">
                                              Looks good!
                                          </div> -->
                                      </div>
                                  </div>
                                  
                                 

                                  <div class="form-group row">
                                      <div class="col-lg-offset-2 col-lg-10">
                                          <button class="btn btn-danger" name="submit" value="csv" type="submit">{{ __('form.action.Submit')}}</button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </section>
                  </div>
              </div>
              
             
              <!-- page end-->
          </section>
      </section>
      @endsection
@section('js')
@endsection