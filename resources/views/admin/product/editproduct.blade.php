@extends('layout.master')
@section('css')
@endsection
@section('content')
<section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="card">
                          <header class="card-header">
                             Update Product
                          </header>
                          <div class="card-body">
                              <div class="form">
                              <!-- novalidate="novalidate" -->
                                  <form class="form-horizontal" method="post" action='{{ url("admin/updateproduct/$data->id") }}' >
                                  
                                      {!! csrf_field() !!}
                                      <div class="row"> <!-- start row -->
                                      <div class="col-md-6">
                                      <div class="form-group row">
                                          <label for="firstname" class="control-label col-lg-2">Container  Code</label>
                                          <div class="col-lg-10">
                                              <input class=" form-control" id="firstname" name="container_code" type="text" value="{{ $data->container_code }}">
                                          </div>
                                      </div>
                                      <div class="form-group row ">
                                          <label for="lastname" class="control-label col-lg-2">Time</label>
                                          <div class="col-lg-10">
                                              <input class=" form-control" id="lastname" name="Time" type="text" value="{{ $data->Time }}">
                                          </div>
                                      </div>
                                      <div class="form-group row ">
                                          <label for="username" class="control-label col-lg-2">item Remark</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="item_remark" name="item_remark" type="text" value="{{ $data->item_remark }}">
                                          </div>
                                      </div>
                                      <div class="form-group row ">
                                          <label for="password" class="control-label col-lg-2">Courier Code</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="courier_code" name="courier_code" type="text" value="{{ $data->courier_code }}">
                                          </div>
                                      </div>
                                      <div class="form-group row ">
                                          <label for="confirm_password" class="control-label col-lg-2">Courier Symbol</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="confirm_password" name="courier_symbol" type="text" value="{{ $data->courier_symbol }}">
                                          </div>
                                      </div>
                                    <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">Quantity</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="quantity" name="quantity" type="text" value="{{ $data->quantity }}">
                                          </div>
                                      </div>

                                       <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">rmb</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="rmb" name="rmb" type="rmb" value="{{ $data->rmb }}">
                                          </div>
                                      </div>
                                      </div>
                                      <div class="col-md-6">

 <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">rm</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="rm" name="rm" type="text" value="{{ $data->rm }}">
                                          </div>
                                      </div>


                                   <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">Size</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="size" name="size" type="text" value="{{ $data->size }}">
                                          </div>
                                      </div>

                                       <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">Courier fee</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="courier_fee" name="courier_fee" type="text" value="{{ $data->courier_fee }}">
                                          </div>
                                      </div>

                                       <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">Amount</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="amount" name="amount" type="text" value="{{ $data->amount }}">
                                          </div>
                                      </div>

                                          <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">Remark</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="remark" name="remark" type="text" value="{{ $data->remark }}">
                                          </div>
                                      </div>

                                      <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">Arrival Date</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="arrival_date" name="arrival_date" type="text" value="{{ $data->arrival_date }}">
                                          </div>
                                      </div>

                                      <div class="form-group row ">
                                          <label for="email" class="control-label col-lg-2">Client Code</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="client_code" name="client_code" type="text" value="{{ $data->arrival_date }}">
                                          </div>
                                      </div>
                                      </div>
                                
                                      <div class="form-group">
                                          <div class="col-md-12">
                                              <button class="btn btn-danger" type="submit">Save</button>
                                              <button class="btn btn-default" type="button">Cancel</button>
                                          </div>
                                      </div>
                                     
                                    </div><!-- end row -->
                                  </form>
                              </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>

      @endsection
@section('js')
@endsection

