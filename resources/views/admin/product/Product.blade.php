@extends('layout.master')
@section('css')
<link rel="stylesheet" href="{{ asset('assets/select2/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/buttons.bootstrap4.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/select.bootstrap4.min.css') }}"/>
<!-- <link rel="stylesheet" href="{{ URL::asset('assets/jquery-datatables-checkboxes/css/dataTables.checkboxes.css') }}"> -->
<style>
table.display tr.odd.selected,table.display tr.even.selected{
    background: #f4f0eb !important;
    color:#000;
}
.select2-container .select2-selection--single{
    height: 38.2px !important;
    border: 1px solid #ced4da;
}
.select2-container--default .select2-selection--multiple {
    border: 1px solid #ced4da;
}
.select2-container .select2-selection--multiple{
    min-height: 38.2px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered{
    line-height: 38px !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__rendered{
    line-height: 24px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow b{
    top: 70% !important;
}
table.display tr.trash.gradeX
{
    color:#9d9d91;
    background:#fbcccc !important;
}
#datarow{
  border: 2px solid #324;
  border-radius: 20px;
  width: 175px;
  margin-bottom: 10px;
  text-align:center;
  font-size:14px;
  line-height:1.2;
  padding:8px;
  display:none;
}

</style>
@endsection
@section('content')
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
@include('forms.form-status')
@include('forms.form-add-courier')
              <!-- page start-->
              <div class="fixed-bottom" style="margin: 10px;">
              <div id="datarow">Selected 10 item <br>total 10</div>
              <a href="{{ url('admin/ProductList?d=rb') }}" title="Product trash"><i class="fa fa-trash-o fa-5x"></i> </a>
              
              </div>
              <div class="row">
                <div class="col-sm-12 overflow-auto">
              <section class="card">

              <header class="card-header">
                  {!! $menu !!}
             <span class="tools pull-right">
             <button type="button" data-toggle="modal" data-target="#mcourier" class="btn btn-info"><i class="fa fa-plus"></i> {{ __('form.action.add_courier_code') }}</button>

                {{--<a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>--}}
             </span>
              </header>
            
              <div class="card-body overflow-auto">
              <div class="adv-table">
              @if(!$is_trash)
              
              <div class="clearfix">
                    <!-- start time -->
                    <form id="getform" method="GET">                 
                    <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">{{ __('form.action.From')}}</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">{{ __('form.action.From')}}</div>
                                    </div>
                                    <input type="text" class="form-control rounded fd1" id="fdate" placeholder="Time" name="startdate1">
                                </div>
                            </div>
                        <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">{{ __('form.action.To')}}</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">{{ __('form.action.To')}}</div>
                                    </div>
                                    <input type="text" class="form-control rounded fd2" id="tdate" placeholder="Time" name="enddate1">
                                </div>
                            </div>
                        <div class="col-auto">
                    
                                <div class="input-group mb-2" style="width:200px;">
                                <select id="tclient1" class="form-control" name="tclient1[]" multiple="multiple"> 
                                    <option value="">Client Code</option>
                                    @foreach($client_codes as $code)
                                        <option value="{{ $code->client_code }}">{{ $code->client_code }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">{{ __('form.Status')}}</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">{{ __('form.Status')}}</div>
                                    </div>
                                <select id="tstatus1" class="form-control" name="status1">
                                    <option value="">All</option>
                                    <option value="Shipping" >Shipping</option>
                                    <option value="Arrival" >Arrival</option>
                                    <option value="Collected">Collected</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">{{ __('form.action.Action')}}</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">{{ __('form.action.Action')}}</div>
                                    </div>
                                <select id="taction1" class="form-control" name="by1">
                                    <option value="search" >Search</option>
                                    <option value="excel">Export</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <button id="submitfilter" type="button" onclick="submitForm()" class="btn btn-primary mb-2">{{ __('form.action.Submit')}}</button>
                            </div>
                        </div>
                        </form>
                    <!-- end time -->
              </div>
            
              {{--<h3>Old search filter method</h3>
              <div class="clearfix">
                    <!-- start time -->
                <form action="{{ route('admin.excel',['action'=>'export','type'=>'product'])}}" method="POST">
                    @csrf
                    <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">{{ __('form.action.From')}}</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">{{ __('form.action.From')}}</div>
                                    </div>
                                    <input type="text" class="form-control rounded fd1" id="fdate" placeholder="Time" name="fdate">
                                </div>
                            </div>
                        <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">{{ __('form.action.To')}}</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">{{ __('form.action.To')}}</div>
                                    </div>
                                    <input type="text" class="form-control rounded fd2" id="tdate" placeholder="Time" name="tdate">
                                </div>
                            </div>
                        <div class="col-auto">
                    
                                <div class="input-group mb-2" style="width:200px;">
                                <select id="tclient" class="form-control select_client" name="tclient[]" multiple="multiple"> 
                                    <option value="">Client Code</option>
                                    @foreach($client_codes as $code)
                                        <option value="{{ $code->client_code }}">{{ $code->client_code }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">{{ __('form.Status')}}</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">{{ __('form.Status')}}</div>
                                    </div>
                                <select id="tstatus" class="form-control" name="tstatus">
                                    <option value="">All</option>
                                    <option value="Shipping" >Shipping</option>
                                    <option value="Arrival" >Arrival</option>
                                    <option value="Collected">Collected</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">{{ __('form.action.Action')}}</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">{{ __('form.action.Action')}}</div>
                                    </div>
                                <select id="taction" class="form-control" name="by">
                                    <option value="search" >Search</option>
                                    <option value="excel">Export</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <button id="tsubmit" type="submit" class="btn btn-primary mb-2">{{ __('form.action.Submit')}}</button>
                            </div>
                        </div>
                        </form>
                    <!-- end time -->
              </div>--}}
              @endif
              <div class="clearfix">
                    <div class="btn-group">
                    <button id="btnSelectedRows" type="button" class="btn btn-primary"><i class="fa fa-check-square"></i> Size selected</button>
                    <button type="button"  data-toggle="modal" data-target="#selecsts" class="btn btn-info"><i class="fa fa-retweet"></i> {{ __('table.action.change_status') }}</button>
                              <button type="button" id="delete" class="btn btn-danger"><i class="fa fa-trash-o"></i> {{ __('table.action.bulk_delete') }} </button>
                            @if($is_trash)  
                            <button type="button" id="restore1" class="btn btn-warning"><i class="fa fa-reply"></i> {{ __('table.action.bulk_restore') }} </button>
                            @else
                            <a type="button" class="btn btn-warning" style="color:white" href="{{ url('admin/ProductList?d=cd') }}"><i class="fa fa-exchange"></i> {{ __('table.action.check_duplicate') }}</a>

                            @endif
                            
                    </div>
                    
                    <div class="pull-right" style="display: inline-flex;">
                    <div class="custom-control custom-checkbox mb-3" style="padding-right: 10px;margin-top: 10px;">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="monthfilter" @if($isMonthFilter) checked="checked" @endif onchange="changeFilter(this)">
                    <label class="custom-control-label" for="customCheck">3 Months Older</label>
                    </div>
                    <!-- <div class="btn-group> -->
                    @if($is_trash)
                        <a class="btn btn-success" onclick="return confirm('Are you sure Restore all data?')" href="{{ route('submit.action',['type'=>'undoall','db'=>'product']) }}" title="Revert All Product"><i class="fa fa-undo"></i> {{ __('table.action.restore_all') }}</a>
                    @else
                    
                
                        <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gears"></i> {{ __('table.action.tools') }} 
                        </button>
                        <ul class="dropdown-menu pull-right" style="padding: 3px;">
                            <li>
                            <form id="import-excel" action='' method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input id="upload-product" type="file" name="upload_product" style="display:none;"/>
                            </form>
                            <a href="#" id="upload-link"><i class="fa fa-upload"></i> {{ __('table.action.import_product') }}</a></li>
                            <li>
                            <a href="#" onclick="event.preventDefault();document.getElementById('down-excel').submit();"><i class="fa fa-rocket"></i>  {{ __('table.action.export_product') }}
                                    </a> 
                            <form id="down-excel" action="{{ route('admin.excel',['action'=>'export','type'=>'product','by'=>'all'])}}" method="POST" style="display: none;">
                                        @csrf
                                    </form> 
                            </li>
                            <li><a href="{{ URL::asset('data/product-template.xlsx') }}"><i class="fa fa-download"></i> {{ __('table.action.template') }}</a></li>
                            <li><a href="{{ URL::asset('data/product-template-csv.csv') }}"><i class="fa fa-download"></i> {{ __('table.action.template_csv') }}</a></li>
                        </ul>
                        @endif
                    <!-- </div> -->
                    </div>
                </div>
                <div class="space15"></div>
              <table  class="display table table-bordered" style="width:100%" id="tproduct">
              <thead>
              <tr>
              <th class="no-sort" style="width:10px"> <input id="mass" type="checkbox"></th>
                <th>{{ __('table.Container Code') }}</th>
                <th>{{ __('table.Time') }}</th>
                <th>{{ __('table.Scan Time') }}</th>
                <th>{{ __('table.item Remark') }}</th>
                <th>{{ __('table.Discount') }}</th>
                <th>{{ __('table.Client Code') }}</th>
                <th>{{ __('table.Courier Symbol') }}</th>
                <th>{{ __('table.Quantity') }}</th>
                <th>{{ __('table.rmb') }}</th>
                <th>{{ __('table.rm') }}</th>
                <th>{{ __('table.Weight') }}</th>
                <th>{{ __('table.Size') }}</th>
                <th>{{ __('table.Courier fee') }}</th>
                <th>{{ __('table.Amount') }}</th>
                <th>{{ __('table.Remark') }}</th>
                <th>{{ __('table.Arrival Date') }}</th>
                <th>{{ __('table.Courier Code') }}</th>
                <th>{{ __('table.Status') }}</th>
                  <th class="no-sort">{{ __('table.action.action') }}</th>
               
              </tr>
              </thead>
              <tfoot>
		<tr>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th style="text-align:right" rowspan="1" colspan="2"></th>
        <th style="text-align:right" rowspan="1" colspan="2"></th>
        <!-- <th rowspan="1" colspan="1"></th> -->
        <!-- <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th> -->
        </tr>
        <!-- <th rowspan="1" colspan="2"></th> -->
	</tfoot>
              <tbody>
                 <?php $i=0; ?>    @foreach($product as $data)
              <tr class="gradeX {{ $is_trash ? 'trash':'' }}" id="{{ $data->id }}">
              <td></td>
                  <td id="container_code" class="edit">{{ $data->container_code }}</td>
                  <td>{{ $data->Time }}</td>
                  <td>{{ $data->scan_time }}</td>
                  <td id="item_remark" class="edit">{{ $data->item_remark }}</td>
                  <td id="discount" class="edit">{{ $data->discount }}</td>
                  <td id="client_code" class="edit">{{ $data->client_code }}</td>
                  <td id="courier_symbol" class="edit">{{ $data->courier_symbol }}</td>
                  <td id="quantity" class="edit">{{ $data->quantity }}</td>
                  <td id="rmb" class="edit">{{ $data->rmb }}</td>
                  <td id="rm" class="edit_decimal">{{ $data->rm }}</td>
                  <td id="weight" class="edit">{{ $data->weight }}</td>
                  <td id="size" class="edit_number">{{ $data->size }}</td>
                  <td id="courier_fee" class="edit">{{ $data->courier_fee }}</td>
                  <td id="amount" class="edit_decimal">{{ $data->amount }}</td>
                  <td id="remark" class="edit">{{ $data->remark }}</td>
                  <td id="arrival_date" class="editdate">{{ $data->arrival_date }}</td>
                  <td id="courier_code" class="edit">{{ $data->courier_code }}</td>
                  <td>                                 
                    <select id="st{{$data->id}}" class="form-control" name="status" onchange="tStat({{$data->id}})">
                    <option value="">--Status--</option>
                    <option value="Shipping" <?php if($data->status=='Shipping'){echo 'selected';} ?>>Shipping</option>
                    <option value="Arrival" <?php if($data->status=='Arrival'){echo 'selected';} ?>>Arrival</option>
                    <option value="Collected" <?php if($data->status=='Collected'){echo 'selected';} ?>>Collected</option>
                  </select>
                </td>
                  <td>
                  @if($is_trash)
                  <div class="btn-group-horisontal text-center">
                   <a class="btn btn-danger" title="Delete permanently" style="padding: 0px 4px;" onclick="return confirm('Are you sure to delete permanently? You will not be able to undo this!')" href="{{ route('submit.action',['id'=>$data->id,'type'=>'permanent','db'=>'product']) }}"><i class="fa fa-trash-o "></i></a>
                   <a class="btn btn-info" title="Restore" style="padding: 0px 4px;" onclick="return confirm('Are you sure Restore this data?')" href="{{ route('submit.action',['id'=>$data->id,'type'=>'undo','db'=>'product']) }}"><i class="fa fa-undo "></i></a>
                  </div>
                  @else
                  <a class="btn btn-danger btn-xs" onclick="return confirm('Are you sure delete this?')" href="{{ route('submit.action',['id'=>$data->id,'type'=>'delete','db'=>'product']) }}"><i class="fa fa-trash-o "></i></a>
                  @endif
                  </td>
              </tr>
             
              @endforeach
              </tbody>
            
              </table>
             {{-- <div class="d-flex">
                <div class="mx-auto">
                {{$product->links("pagination::bootstrap-4")}}
                </div>
            </div>--}}
              </div>
              </div>
             
              </section>
              </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!-- Right Slidebar start -->
      @endsection
@section('js')
<script src="{{ URL::asset('assets/advanced-datatable/examples/examples_support/jquery.jeditable.datepicker.min.js')}}"></script>
<script src="{{ asset('assets/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/buttons.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/dataTables.select.min.js') }}"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.colVis.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script> -->
<script>
// document.getElementById('stat1').onchange = function () {
//         // access text property of selected option
//         el.value = sel.options[sel.selectedIndex].text;
//     }
$(document).ready(function() {
    // $('.select_client').select2({
    // placeholder: "Select client code"
    // });
    $('#tclient1').select2({
        placeholder: "Select client code"
    });
});

function submitForm(){
    let betweenDate = "filter[starts_between]="+$('input[name="startdate1"]').val()+','+$('input[name="enddate1"]').val();
    let status = "filter[status]="+$("#tstatus1 :selected").val();
    //let tclient = $('input[name="tclient1"]').val();
    let by = $("#taction1 :selected").val();
    var bc = "filter[client_code]="+$('#tclient1').select2("val");
    let data = betweenDate+"&"+bc+"&"+status;
    let url = "{{ route('admin.filter')}}"+"?action="+by+"&"+data;
    // console.log('betwenn: '+ betweenDate);
    // console.log('status: '+ status);
    // console.log('by: '+ by);
    // console.log('bc: '+ bc);
    // console.log('url: '+ url);
    window.location.replace(url);

}
 
 function tStat(id) {
        let token = '{{ csrf_token() }}';
        let formx = new FormData();
        let status = document.getElementById('st'+id).value;
        formx.append('_token',token);
        formx.append('status',status);
        //console.log(status);
        $.ajax({
            url: '../onchangestatus/'+id,
            type : 'POST',
            data : formx,
            processData : false,
            contentType : false,
            cache: false,
            timeout: 2000,
            success : function(data) {
                console.log(data);
                if(data.code==0){
                    toastr.success(data.message, 'Info')
                    window.setTimeout(function () { location.reload(true); }, 1000);
                }else{
                    toastr.warning('Something wrong', 'Info')
                }
            }
        }); 
    }
   $(function(){
    @if($isMonthFilter)
    var date1 = new Date();
   var today1 = new Date(date1.getFullYear(), date1.getMonth()-2, date1.getDate());
   @else
    var today1 = '2020-01-01';
   @endif
    var can1 = $('.fd2').datepicker({
        format: 'yyyy-mm-dd',
        minDate: today1,
        startDate: today1,
        onRender: function(date) {
            return date.valueOf() <= cin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        can1.hide();
    }).data('datepicker');   
    var cin = $('.fd1').datepicker({
        format: 'yyyy-mm-dd',
        minDate: today1,
        startDate: today1,
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() > can1.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            can1.setValue(newDate);
        }
        cin.hide();
        $('.fd2')[0].focus();
    }).data('datepicker');

    //console.log(can1);
    $("#upload-link").on('click', function(e){
        e.preventDefault();
        if(confirm("File format must type .xlsx")){
            $("#upload-product:hidden").trigger('click');
            e.preventDefault();
        }
    });
  
    $("#upload-product:hidden").on('change', function(e){
      
        //let d = $(this).val();
        //let token = '{{ csrf_token() }}';
        //console.log(d);
        //console.log("_token: " + token);
        toastr.info('Uploading....', 'Info');
        var form = $('#import-excel')[0];
        var form1 = new FormData(form);
        //let formData = new FormData();
        //formData.append('prod',d);
        //formData.append('_token',token);
        $.ajax({
            url: '{{ route('admin.excel',['action'=>'import','type'=>'product'])}}',
            type : 'POST',
            enctype: 'multipart/form-data',
            data : form1,
            processData : false,
            contentType : false,
            cache: false,
            timeout: 86400 * 2,
            success : function(data) {
                console.log(data);
                if (typeof data.response !== 'undefined') 
                {
                    if (data.response == 'false') 
                    {
                        toastr.warning(data.message, 'Info')
                        return;                        
                    }
                }
                toastr.success(data.message, 'Info')
                var delay = 3000;
                window.setTimeout(function () { location.reload(true); }, delay);
                
            
            }
        });
        // if(confirm("File format must type .xlsx")){
        //     $("#upload-product:hidden").trigger('click');
        // }
        e.preventDefault();
    });

});
// /uploadProduct
    $.editable.addInputType( 'datetimepicker', {
element: function (settings, original) {
var form = $(this),
    input = $('<input />');
input.attr('autocomplete', 'off');
form.append(input);
return input;
},
plugin: function (settings, original) {
var form = this,
    input = form.find("input");
settings.onblur = 'nothing';
datetimepicker = {
    onSelect: function () {
        form.submit();
    },
    onClose: function () {
        setTimeout(function () {
            if (!input.is(':focus')) {
                original.reset(form);
            } else {
                form.submit();
            }
        }, 150);
    }
};

if (settings.datetimepicker) {
    jQuery.extend(datetimepicker, settings.datetimepicker);
}

input.datetimepicker(datetimepicker);
}
});
function isInteger(n) {
    return n === +n && n === (n|0);
}
function isFloat(n) {
    return n === +n && n !== (n|0);
}
var numbersOnly = function (e, decimal) {
    var key;
    var keychar;

            if (window.event) {
            key = window.event.keyCode;
            }
            else if (e) {
            key = e.which;
            }
            else {
            return true;
            }
            keychar = String.fromCharCode(key);
        
            //0= winkey, 8= backspace, 9= tab, 13= enter, 27= escape
            if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
            return true;
            }
            else if ((("0123456789-").indexOf(keychar) > -1)) {
            return true;
            }
            else if (decimal || (keychar == ".")) {
            return true;
            }
            else{
                toastr.warning("number only please");
            return false;
            }
 };
$.editable.addInputType('dernumber', {
    element: $.editable.types.text.element,
    plugin: function (settings, original) {
      $('input', this).bind('keypress', function (event) {
       return numbersOnly(event, false);
      });
    },
    submit : function (settings, original) {
        var vad = $('input', this).val();
        if(vad < 999.99 && vad > 0.00000){ 
            return true;
        }else{
            toastr.warning("Size must between 0.01 - 999.99")
            return false;
        }
       // return true;
    }
});
$.editable.addInputType('decnumber', {
    element: $.editable.types.text.element,
    plugin: function (settings, original) {
      $('input', this).bind('keypress', function (event) {
       return numbersOnly(event, false);
      });
    },
    submit : function (settings, original) {
        var vad = $('input', this).val();
        // if(vad < 999.99 && vad > 0.00000){ 
        //     return true;
        // }else{
        //     toastr.warning("Size must between 0.01 - 999.99")
        //     return false;
        // }
        return true;
    }
});

$.editable.addInputType('number', {
  element : function(settings, original) {
      var input = $('<input type="number">');
      $(this).append(input);
      return(input);
  },
  submit : function (settings, original) {
      //if (isNaN($(original).val())) {
        //toastr.warning('You must provide a number!', 'Alert')
          //alert('You must provide a number')
          console.log('sini true '+settings)
         // return false;
     // } else {
      //  console.log('sini false'+$(original).val())
        //  return true;
      //}
  }
});

</script>
<script>
    function isNumeric(value) {
    if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/)) return false;
    return true;
    }
    function changeFilter(cbx){
        let isChecked = cbx.checked ? '1':'0';
        let fdx = new FormData();
            fdx.append('keyname','use_month_filter');
            fdx.append('keyvalue',isChecked);
            $.ajax({
                url: '{{ route('api.changeSetting') }}',
                type : 'POST',
                data : fdx,
                processData : false,
                contentType : false,
                success : function(data) {
                    console.log(data);
                    if (typeof data.code !== 'undefined') 
                    {
                        if (data.code == 200) 
                        {
                            toastr.success(data.message, 'Info')
                            window.setTimeout(function () { location.reload(true); }, 1000);
                            return;                        
                        }else{
                            toastr.warning(data.message, 'Info')
                            return;      
                        }
                    }
                   
                   
                }
            });
    
    }
    function selse(){
        var dop = document.getElementById("sts").value;
        return dop;
    }
    var buttonCommon = {
            exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,17,18],
                rows: { selected: true },
                format: {
                    body : (data, row, col, node) => {
                        if (col == 17) {
                            return $(data).find("option:selected").text()
                        } else return data
                    }
                }
               
            }
        };
    $(document).ready(function() {
    // $('#tproduct tr td').on( 'click', '.place_checkbox', function () {
    //     if(this.checked==true)
    //     {
    //         $(this).closest('tr').addClass('row_selected');
          
    //     }else{
    
    //         $(this).closest('tr').removeClass('row_selected');
    //     }
    // } );
    const checkbox = $("#mass");
   
    // checkbox.change(function(event) {
    //     var checkbox = event.target;
    //     if (checkbox.checked) {
    //         $('#tproduct tr').addClass('row_selected selected');
    //         $('.place_checkbox').prop('checked', true);
    //     } else {
    //         $('#tproduct tr').removeClass('row_selected selected');
    //         $('.place_checkbox').prop('checked', false);
    //     }
    // });
    $('#delete').click( function() {
        if(confirm("Are you sure you want to delete this selected?")){
            let anSelected = fnGetSelected( oTable );
            //console.log(anSelected);
            let formData = new FormData();
            formData.append('ids',anSelected);
            formData.append('type','delete');
            formData.append('db','product');
            formData.append('trash',{{ $is_trash ? 'true' : 'false' }});
            $.ajax({
                url: '{{ route('api.action') }}',
                type : 'POST',
                data : formData,
                processData : false,
                contentType : false,
                success : function(data) {
                    console.log(data);
                    if (typeof data.response !== 'undefined') 
                    {
                        if (data.response == 'false') 
                        {
                            toastr.warning(data.message, 'Info')
                            return;                        
                        }
                    }
                    toastr.success(data.message, 'Info')
                    window.setTimeout(function () { location.reload(true); }, 1500);
                   
                }
            });

        }else{
            return false;
        }
		
	    //var iRow = oTable.fnGetPosition( anSelected[0] );
        //console.log(iRow);
		// oTable.fnDeleteRow( iRow );
	} );
    $('#update').click( function() {
        if(confirm("Are you sure you want to update selected?")){
            let anSelected = fnGetSelected( oTable );
            let formData = new FormData();
            formData.append('ids',anSelected);
            formData.append('type','status');
            formData.append('sts',selse());
            formData.append('db','product');
            console.log(formData);
            $.ajax({
                url: '{{ route('api.action') }}',
                type : 'POST',
                data : formData,
                processData : false,
                contentType : false,
                success : function(data) {
                    if (typeof data.response !== 'undefined') 
                    {
                        if (data.response == 'false') 
                        {
                            toastr.warning(data.message, 'Info')
                            return;                        
                        }
                    }
                    toastr.success(data.message, 'Info')
                    var delay = 1500;
                    window.setTimeout(function () { location.reload(true); }, delay);
                }
            });

        }else{
            return false;
        }
	} );
   
    $('#addcourier').click( function() {
            let formid = 'fAddcourier';
            let fta = $('#'+formid)[0];
            let formData = new FormData(fta);
            let bvc = $('#'+formid).find('input[name=courier_code]').val();
            let bc = $('#'+formid).find('input[name=size]').val();
           // console.log(bvc);
            if(bvc==''){
                alert('Courier code must filled')
                return;
            }
            if(bc!=''){
                if(!(bc < 999.99 && bc > 0.00000)){ 
                    toastr.warning("Size must between 0.01 - 999.99")
                    return;
                }

            }
            // console.log(formData);
            $.ajax({
                url: '{{ route('api.addorupdate') }}',
                type : 'POST',
                data : formData,
                processData : false,
                contentType : false,
                success : function(data) {
                    //console.log(data);
                    if (typeof data.response !== 'undefined') 
                    {
                        if (data.response == 'false') 
                        {
                            toastr.warning(data.message, 'Info')
                            return;                        
                        }
                        if(data.code == '201'){
                            if(confirm(data.message)){
                                $('#is_update').val('true');
                                $('#addcourier').trigger('click'); 
                            }else{
                                toastr.info('You cancel this action', 'Info');
                            }
                            return;
                        }
                        if(data.code == '202'){
                            if(confirm(data.message)){
                                $('#is_restore').val('true');
                                $('#addcourier').trigger('click'); 
                            }else{
                                toastr.info('You cancel this action', 'Info');
                            }
                            return;
                        }
                    }
                    $('#mcourier').modal('toggle'); 
                  toastr.success(data.message, 'Info')
                  window.setTimeout(function () { location.reload(true); }, 1000);
                }
            });    
	} );
    $('#restore1').click( function() {
        if(confirm("Are you sure you want to restore selected?")){
            let anSelected = fnGetSelected( oTable );
            let formData = new FormData();
            formData.append('ids',anSelected);
            formData.append('type','restore');
            formData.append('db','product');
            //console.log(formData);
            $.ajax({
                url: '{{ route('api.action') }}',
                type : 'POST',
                data : formData,
                processData : false,
                contentType : false,
                success : function(data) {
                    if (typeof data.response !== 'undefined') 
                    {
                        if (data.response == 'false') 
                        {
                            toastr.warning(data.message, 'Info')
                            return;                        
                        }
                    }
                    toastr.success(data.message, 'Info')
                    var delay = 1500;
                    window.setTimeout(function () { location.reload(true); }, delay);
                }
            });

        }else{
            return false;
        }
	} );
    function fnGetSelected( oTableLocal ){
        var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('selected') )
		{
			aReturn.push( aTrs[i].getAttribute('id') );
		}
	}
	return aReturn;
        //return oTableLocal.$('tr.row_selected');
    } 
    
    var oTable = $('#tproduct').dataTable({
        "order": [],
        "lengthMenu": [[50, 75, 100, 200, -1], [50, 75, 100, 200, "All"]],
        "pageLength": 200,
        "columnDefs": [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        dom: 'lBfrtip',
        buttons: [
                
                $.extend(true, {}, buttonCommon, {
                    extend: "copy",
                    exportOptions: {
                        columns: [ 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
                    }
                }),
                $.extend(true, {}, buttonCommon, {
                    extend: "csvHtml5",
                    fieldBoundary: "",
                    exportOptions: {
                        columns: [ 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 ]
                    }
                }), 
                $.extend(true, {}, buttonCommon, {
                    extend: "excel",
                    autoFilter: true,
                    sheetName: "Exported product",
                    exportOptions: {
                        columns: [ 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
                    }
                    // exportOptions: {
                    // columns: ':visible:not(.not-exported)',
                    //     modifier: {
                    //         selected: true
                    //     }
                    // },
                }),
                //"colvis",
                
            ],            
            select: {
            style:    'multi',
            selector: 'td:first-child',  
        },
        //"bStateSave": true,
        "fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
            var iTotalsize = 0;
            for ( var i=0 ; i<aData.length ; i++ )
            {
                iTotalsize += aData[i][12]*1;
            }
            var iTotaldisplay = 0;
           // if(iEnd!=0){
            //var io = explode    
            for ( var i=iStart ; i<iEnd ; i++ )
            {
                iTotaldisplay += aData[aiDisplay[i]][12]*1;
            }
            //}
            nFoot.getElementsByTagName('th')[16].innerHTML = "<h6>Size show: "+iTotaldisplay.toFixed(3)+"</h6>";
            nFoot.getElementsByTagName('th')[17].innerHTML = "<h6>Size total: "+iTotalsize.toFixed(3)+"</h6>";
            // nFoot.getElementsByTagName('th')[2].innerHTML = "display: "+aiDisplay[0];
            // nFoot.getElementsByTagName('th')[3].innerHTML = "iStart: "+iStart;
            // nFoot.getElementsByTagName('th')[4].innerHTML = "iEnd: "+iEnd;

		},
        "fnInitComplete": function () {
            this.api().columns([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]).every( function () {
                var column = this;
                var select = $('<select style="width:100%;"><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                            
                        );
                       
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    var val = $.fn.dataTable.util.escapeRegex(d);
                    if (column.search() === "^" + val + "$") {
                    select.append(
                        '<option value="' + d + '" selected="selected">' + d + "</option>"
                    );
                    } else {
                    select.append('<option value="' + d + '">' + d + "</option>");
                    }
                                } );
                            } );
        }
    });
//     $(".select-checkbox:checked", oTable.fnGetNodes()).each(function () {
//         console.log('hii');
//         console.log($(this).val());
//   });
    oTable.$('.edit_number').editable( '{{ route('api.saveInput')}}', {
        "callback": function( sValue, y ) {
            var aPos = oTable.fnGetPosition( this );
            oTable.fnUpdate( sValue, aPos[0], aPos[1] );
        },
        "submitdata": function ( value, settings ) {
            //console.log(value);
            return {
                "row_id": this.parentNode.getAttribute('id'),
                "column": oTable.fnGetPosition( this )[2]
            };
        },
        placeholder:"",
        type:"dernumber",
        submit : "Save",
        "height": "25px",
        "width": "100%"
    });
    oTable.$('.edit_decimal').editable( '{{ route('api.saveDec')}}', {
        "callback": function( sValue, y ) {
            var aPos = oTable.fnGetPosition( this );
            oTable.fnUpdate( sValue, aPos[0], aPos[1] );
        },
        "submitdata": function ( value, settings ) {
            return {
                "row_id": this.parentNode.getAttribute('id'),
                "column": oTable.fnGetPosition( this )[2]
            };
        },
        placeholder:"",
        type:"decnumber",
        submit : "Save",
        "height": "25px",
        "width": "100%"
    });
 
    oTable.$('.edit').editable( '{{ route('api.saveInput')}}', {
        "callback": function( sValue, y ) {
            var aPos = oTable.fnGetPosition( this );
            oTable.fnUpdate( sValue, aPos[0], aPos[1] );
        },
        "submitdata": function ( value, settings ) {
            return {
                "row_id": this.parentNode.getAttribute('id'),
                "column": oTable.fnGetPosition( this )[2]
            };
        },
        placeholder:"",
        submit : "Save",
        "height": "25px",
        "width": "100%"
    });

    oTable.$('.editdate').editable( '{{ route('api.saveInput')}}', {
        "callback": function( sValue, y ) {
            var aPos = oTable.fnGetPosition( this );
            oTable.fnUpdate( sValue, aPos[0], aPos[1] );
        },
        "submitdata": function ( value, settings ) {
            return {
                "row_id": this.parentNode.getAttribute('id'),
                "column": oTable.fnGetPosition( this )[2]
            };
        },
        type      : "datetimepicker",
        datetimepicker : {
            format: "yyyy-mm-dd h:i:s"
        },
        placeholder:"",
        submit : "Save",
        "height": "25px",
        "width": "100%"
    });
    $("#mass").on( "click", function(e) {
    if ($(this).is( ":checked" )) {
        oTable.api().rows( { search: 'applied' } ).select();        
    } else {
        oTable.api().rows().deselect(); 
    }
});
    // $(".select-checkbox").on( "click", function(e) {
    //     console.log('tess')
    // });
    var tow = 0;
    var rw = 0;
    oTable.on('click', 'tr', function () {
        var dd = oTable.api().rows(this).data();
        var dtshow = $("#datarow");  
     if ($(this).hasClass('selected')) {
       // console.log('remove selected');
        tow -= dd[0][12]*1;
        rw--;
     } else {
        //console.log('add selected');
        tow += dd[0][12]*1;
        rw++;
     }
   
     var tba = oTable.api().rows('.selected').count();
     var ssi = 'Selected '+rw+' items, Size Total : <strong>'+tow.toFixed(3)+'</strong>';
     if(rw!=0){
        dtshow.show();
        dtshow.html(ssi);
     }else{
        dtshow.hide();
         
     }
     //console.log(('Selected '+rw+' items, Size Total : '+tow.toFixed(3)));
    });
    // oTable.$('.select-checkbox').on("click",function () {
    //     if ($(this).hasClass('selected')) {
    //       //$(this).removeClass('selected');
    //       console.log('remove selected');   
    //     } else {
    //         //oTable.$('tr.selected').removeClass('selected');
    //         //$(this).addClass('selected');
    //       console.log('add selected');   

    //     }
    //     var tf = 0;
    //    let tgh = oTable.api().rows('.selected').data();
    //       $.each(tgh, function(i, val) {
    //         tf += tgh[i][12]*1;   
    //       }); 
        
   // });
   
    $('#btnSelectedRows').on('click', function() {
        var tb = oTable.api().rows('.selected');
        var tblData = tb.data();
        var selected = tb.count();
        var tmpData = 0;
        $.each(tblData, function(i, val) {
            tmpData += tblData[i][12]*1;
        }); 
        alert('Selected '+selected+' items\n\nSize Total : '+tmpData.toFixed(3));
        console.log(tmpData);
    })

    })

</script>    
@endsection
