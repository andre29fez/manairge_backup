@extends('layout.master')
@section('css')
@endsection
@section('content')
<section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="card">
                          <header class="card-header">
                             {{__('form.Update User Details')}}
                          </header>
                          <div class="card-body">
                              <div class="form">
                                  <form class="cmxform form-horizontal tasi-form" id="signupForm" method="post" action='{{ url("updateuser/$data->id") }}' novalidate="novalidate">
                                      {!! csrf_field() !!}
                                      <div class="form-group row {!! $errors->has('name') ? 'has-error' : '' !!}">
                                          <label for="name" class="control-label col-lg-2">{{__('form.Name')}}</label>
                                          <div class="col-lg-10">
                                              <input class=" form-control" id="name" name="name" type="text" value="{{ $data->name }}">
                                              {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group row {!! $errors->has('email') ? 'has-error' : '' !!}">
                                          <label for="email" class="control-label col-lg-2">{{__('form.Email')}}</label>
                                          <div class="col-lg-10">
                                              <input class=" form-control" id="email" name="email" type="text" value="{{ $data->email }}" readonly>
                                              {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group row {!! $errors->has('phone') ? 'has-error' : '' !!}">
                                          <label for="phonr" class="control-label col-lg-2">{{__('form.Phone')}}</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="phone" name="phone" type="text" value="{{ $data->phone }}">
                                              {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                          </div>
                                      </div>
                                     {{-- <div class="form-group row {!! $errors->has('password') ? 'has-error' : '' !!}">
                                          <label for="password" class="control-label col-lg-2">{{__('form.Password')}}</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="password" name="password" type="password" value="{{ $data->password }}">
                                              {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                          </div>
                                      </div>--}}
                                          <div class="form-group row {!! $errors->has('remark') ? 'has-error' : '' !!}">
                                          <label for="remark" class="control-label col-lg-2">{{__('form.Remark')}}</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="remark" name="remark" type="text" value="{{ $data->remark }}">
                                              {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group row {!! $errors->has('client_code') ? 'has-error' : '' !!}">
                                          <label for="client_code" class="control-label col-lg-2">{{__('form.Client Code')}}</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="client_code" name="client_code" type="text" value="{{ $data->client_code }}">
                                              {!! $errors->first('client_code', '<p class="help-block">:message</p>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group row {!! $errors->has('client_code_s') ? 'has-error' : '' !!}">
                                          <label for="client_code_s" class="control-label col-lg-2">{{__('form.Client Code_2')}}</label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="client_code_s" name="client_code_s" type="text" value="{{ $data->client_code_s }}">
                                              {!! $errors->first('client_code_s', '<p class="help-block">:message</p>') !!}
                                          </div>
                                      </div>
                                    <div class="form-group row ">
                                          <label for="role" class="control-label col-lg-2">{{__('form.Role')}}</label>
                                          <div class="col-lg-10">
                                            <select class="form-control " id="role" name="role">
                                              <option>---Role---</option>
                                              @foreach($roles as $r)
                                              <option value="{{$r->name}}" <?php if($data->roles[0]->name==$r->name){echo 'selected';} ?>>{{ucfirst($r->name)}}</option>
                                              @endforeach
                                            </select>
                                            {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button class="btn btn-danger" type="submit">{{__('form.action.Save')}}</button>
                                              <button class="btn btn-default" type="button">{{__('form.action.Cancel')}}</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      @endsection
@section('js')
@endsection

