@extends('layout.master')
@section('css')
@endsection
@section('content')
<div id="mConfirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">Confirmation</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" id="form_confirm" class="form">
            <input id="userid" class="form-control" type="hidden" name="userid">
                <div class="form-group">
                    <label for="password">Input your password</label>
                    <input id="password" class="form-control" type="password" name="password">
                </div>
              </form>   
            </div>
            <div class="modal-footer">
                <div class="btn-group" role="group" aria-label="Button group">
                    <button class="btn btn-primary" onclick="submitpass()">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12 overflow-auto">
              <section class="card">
              <header class="card-header">
                  {{ __('menu.users') }}
             <span class="tools pull-right">
               <a  title="edit"  href='{{ url("addusers") }}'><button type="button" class="btn btn-danger "><i class="">{{ __('table.Add User') }}</i></button></a>
             </span>
              </header>
              <div class="card-body overflow-auto">
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" style="width:100%" id="tusers">
              <thead>
              <tr>
                  <th>{{ __('table.name') }}</th>
                  <th>{{ __('table.Client Code') }}</th>
                  <th>{{ __('table.Client_Code_2') }}</th>
                  <th>{{ __('table.Email') }}</th>
                  <th>{{ __('table.Phone') }}</th>
                  <th>{{ __('table.Remark') }}</th>
                  <th>{{ __('table.Role') }}</th>
                  <th>{{ __('table.Created Date') }}</th>
                  <th>{{ __('table.Updated Date') }}</th>
                  <th>{{ __('table.Status') }}</th>
                  <th>{{ __('table.action.action') }}</th>
               
              </tr>
              </thead>
              <tbody>
          @foreach($userData as $data)
              <tr class="gradeX">

                  <td>{{ $data->name }}</td>
                  <td>{{ $data->client_code }}</td>
                  <td>{{ $data->client_code_s }}</td>
                  <td>{{ $data->email }}</td>
                  <td>{{ $data->phone }}</td>
                  <td>{{ $data->remark }}</td>
                  <td>@foreach($data->roles as $k)
                        <button type="button" class="btn btn-info btn-xs" style="padding:0 8px;font-size:12px;">{{ $k->name }}</button>
                      @endforeach
                  </td>
                  <td>{{ $data->created_date }}</td>
                  <td>{{ $data->modified_date }}</td>
                 
                  <td>


      <form action='{{ url("useronchangestatus/$data->id") }}' method="post">
        {!! csrf_field() !!}
                                      
                    <select name="status"  onchange="this.form.submit()">
                    <option value="">--Status--</option>
                    <option value="1" <?php if($data->status=='1'){echo 'selected';} ?>>Active</option>
                    <option value="2" <?php if($data->status=='2'){echo 'selected';} ?>>Ban</option>
                   
                  </select>
     </form>
                </td>
                  <td>
                  <div class="btn-group-horisontal text-center">
                  
                    <a href='{{ url("editusers/$data->id") }}' class="btn btn-warning btn-flat" style="padding: 0px 4px;"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-flat" onclick="actdel({{$data->id}})" href="#" style="padding: 0px 4px;"><i class="fa fa-trash-o "></i></a>
                </div>
                  </td>
              </tr>
             
              @endforeach
              </tbody>
            
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>

              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!-- Right Slidebar start -->
      @endsection
@section('js')
<script>
 var oUsers = $('#tusers').dataTable({
        "order": [[6,"desc"]],
        "columnDefs": [{
        "targets"  : 'no-sort',
        "orderable": false,
        },
        { "width": "10%", "targets": 9 }],
        "pageLength": 25,
    });
function actdel(idx){
  if (confirm("Are you sure delete this?")) {
        $.ajax({
            url: '{{ url('user/submit/delete/admin_login?id=') }}'+idx,
            type : 'GET',
            processData : false,
            contentType : false,
            cache: false,
            timeout: 5000,
            success : function(data) {
                console.log(data);
                if (typeof data.code !== 'undefined') {
                  if(data.code==1){
                    let s= document.getElementById('userid');
                    s.value = data.confirm;
                    $('#mConfirm').modal();
                  }else if(data.code==0){
                    toastr.success(data.confirm, 'Info')
                    var delay = 3000;
                    window.setTimeout(function () { location.reload(true); }, delay);
                  }else{
                    alert('Something wrong')
                  }
               }  
            }
        });
  }
} 

function submitpass(){
    let form = 'form_confirm';
    let popup = 'mConfirm';
    let password = $('#'+form).find('input[name=password]').val();
    let userid = $('#'+form).find('input[name=userid]').val();
    $.ajax({
        url: '/user/submit/delete/admin_login?id='+userid+'&password='+password,
        type : 'GET',
        processData : false,
        contentType : false,
        success : function(data) {
            if (typeof data.code !== 'undefined') 
            {
              $('#'+popup).modal('hide');
                if (data.code != 1) 
                {
                  toastr.warning(data.confirm, 'Info')
                  return;                        
                }
                toastr.success(data.confirm, 'Info')
                var delay = 3000;
                window.setTimeout(function () { location.reload(true); }, delay);
            }               
        }
    });
}
</script>
@endsection
                           