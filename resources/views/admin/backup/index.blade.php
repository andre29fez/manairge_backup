@extends('layout.master')
@section('css')
<style>
.overlay {
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  transform: -webkit-translate(-50%, -50%);
  transform: -moz-translate(-50%, -50%);
  transform: -ms-translate(-50%, -50%);
  color:#1f222b;
  z-index: 9999;
  background: rgba(255,255,255,0.7);
}
#loading{
  display: none;
  position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 50;
    background: rgba(255,255,255,0.7);
}
.overlay{
  background-image: url('{{asset('img/loading.gif')}}') !important;
}
.fa-iz{
  height: 79px !important;
}
</style>
@endsection
@section('content')
<section id="main-content">
          <section class="wrapper">
<!-- page start-->
<div class="row">
<div class="col-sm-12 overflow-auto">
<section class="card">

<header class="card-header">
    {{ __('menu.backup_restore') }}
<span class="tools pull-right">
<a href="javascript:;" class="fa fa-chevron-down"></a>
<a href="javascript:;" class="fa fa-times"></a>
</span>
</header>
<!-- card body -->
<div class="card-body">
</div>
<!-- end card body -->
<!-- card table -->
<div class="card-body overflow-auto">
    <div class="adv-table">
    <!-- BUTTON CONTROL -->
    <div class="clearfix">
    <div class="btn-group pull-right">
      <button type="button" id="generate" onClick="generateBackup($(this));" class="btn btn-info"><i class="fa fa-data"></i> {{ trans('table.generate_now') }}</button>
    </div>
    </div>
    <div class="space15"></div>
    <!-- END BUTTON CONTROL -->
    <!-- DISINI TABLE -->
    <table  class="display table table-bordered" style="width:100%" id="tbackup">
                <thead>
                <tr>
                  <th>{{ trans('table.sort') }}</th>
                  <th>{{ trans('table.date') }}</th>
                  <th>{{ trans('table.name') }}</th>
                  <th>{{ trans('table.size') }}</th>
                  <th>{{ trans('table.download') }}</th>
                  <th>{{ trans('table.remove') }}</th>
                  <th>{{ trans('table.restore') }}</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($arrFiles as $key => $file)
                    <tr>
                     <td>{{ (count($arrFiles) - $key) }}</td>
                     <td>{{ $file['time']}}</td>
                     <td>{{ $file['name']}}</td>
                     <td>{{ $file['size']}}</td>
                      <td>{!! '<a href="?download='.$file['name'].'"><button title="'.trans('table.action.download').'" class="btn btn-flat btn-primary"><span class="fa fa-save"></span> '.trans('table.action.download').'</button ></a>' !!}</td>
                      <td>{!! '<button  onClick="processBackup($(this),\''.$file['name'].'\',\'remove\');" title="'.trans('table.action.remove').'" class="btn btn-flat btn-danger"><span class="fa fa-trash-o"></span> '.trans('table.action.remove').'</button >' !!}</td>
                      <td>{!! '<button  onClick="processBackup($(this),\''.$file['name'].'\',\'restore\');" title="'.trans('table.action.restore').'"  class="btn btn-flat btn-warning"><span class="fa fa-retweet"></span> '.trans('table.action.restore').'</button >' !!}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
    </div>
</div>
<!-- end card table -->
</section>
</div>
</div>
<!-- page end-->
    </section>
</section>
<!--main content end-->
<div id="loading">
          <div id="overlay" class="overlay"><i class="fa fa-pulse fa-5x fa-fw fa-iz"></i></div>
   </div>
@endsection
@section('js')
<script>
    var oBackup = $('#tbackup').dataTable({
        "order": [[0,"asc"]],
        "columnDefs": [{
        "targets"  : 'no-sort',
        "orderable": false,
        },
        { "width": "5%", "targets": 0 }],
        "pageLength": 25,
    });
    function generateBackup(obj) {
      $('#loading').show()
      obj.button('loading');
      $.ajax({
        type: 'POST',
        dataType:'json',
        url: '{{ route('admin.backup.generate') }}',
        data: {
          "_token": "{{ csrf_token() }}",
        },
        success: function (response) {
          console.log(response);
          if(parseInt(response.error) ==0){
            toastr.success(response.msg, 'Info')
            window.setTimeout(function () { location.reload(true); }, 2000);
          }else{
            toastr.warning('Generate failed!', 'Info')
          }
          $('#loading').hide();
          obj.button('reset');
        }
      });

  }   
 function processBackup(obj,file,action) {
  toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Yes</button>",'Are you sure?<br />You won\'t be able to revert this!',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
          $("#confirmationRevertYes").click(function(){
            $('#loading').show()
            obj.button('loading');
            $.ajax({
              type: 'POST',
              dataType:'json',
              url: '{{ route('admin.backup.process') }}',
              data: {
                "_token": "{{ csrf_token() }}",
                "file":file,
                "action":action,
              },
              success: function (response) {
                console.log(response);
                if(parseInt(response.error) == 0){
                  toastr.success(response.msg, 'Info')
                  window.setTimeout(function () { location.reload(true); }, 2000);
                }else{
                  toastr.warning(response.msg, 'Info')
                }
                $('#loading').hide();
                obj.button('reset');
              }
            });
          });
        }
  });
 }
</script>
@endsection