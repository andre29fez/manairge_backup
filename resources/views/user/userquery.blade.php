@extends('layout.master')
@section('content')
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                <div class="col-sm-12 overflow-auto">
              <section class="card">
              
              <header class="card-header">
                  {{ $menu }}
             <span class="tools pull-right">
                <!-- <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a> -->
             </span>
              </header>
                <div class="card-body">
                <form action='{{ url("user/searchdetails") }}' method="post" role="form">
                                  {!! csrf_field() !!}
                                  <div class="form-row align-items-center">
                                        <div class="col-auto">
                                          <label class="sr-only" for="inlineFormInputGroup">{{ __('form.Status') }}</label>
                                          <div class="input-group mb-2">
                                              <div class="input-group-prepend">
                                                  <div class="input-group-text">{{ __('form.Status') }}</div>
                                              </div>
                                          <select class="form-control" name="status">
                                               <option value="">--Select--</option>
                                               <option value="Ordering" >Ordering</option>
                                               <option value="Shipping" >Shipping</option>
                                               <option value="Arrival" >Arrival</option>
                                               <option value="Collected">Collected</option>
                                          </select>
                                             
                                            
                                          </div>
                                      </div>
                                     
                                      <div class="col-auto">
                                          <button type="submit" class="btn btn-primary mb-2">{{ __('form.Search') }}</button>
                                          <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#addcourier">{{ __('form.Add Courier Code')}}</button>
                                      </div>
                                  </div>
                                  </form>
                               <div id="addcourier" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
                                   <div class="modal-dialog modal-dialog-centered" role="document">
                                       <div class="modal-content">
                                           <div class="modal-header">
                                               <h5 class="modal-title" id="my-modal-title">{{ __('form.Add Courier Code')}}</h5>
                                               <button class="close" data-dismiss="modal" aria-label="Close">
                                                   <span aria-hidden="true">&times;</span>
                                               </button>
                                           </div>
                                           <div class="modal-body">
                                           <form method="post" id="form_couirer" class="form">
                                           <input class="form-control" type="hidden" name="userId" id="userId" value="{{Session::get('id')}}">
                                               <div class="form-group">
                                                   <label for="courier_code">{{ __('form.Courier Code') }}</label>
                                                   <input id="courier_code" class="form-control" type="text" name="courier_code">
                                               </div>
                                            </form>   
                                           </div>
                                           <div class="modal-footer">
                                               <div class="btn-group" role="group" aria-label="Button group">
                                                   <button class="btn btn-primary" onclick="submitCourier()">{{ __('form.action.Save') }}</button>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                                 
                          </div>
              <div class="card-body overflow-auto">
              <div class="adv-table">
              <table class="table table-hover table-bordered table-striped" style="width:100%" id="tsubmit">
              <thead>
              <tr>
                  <th>{{ __('table.No') }}</th>
                  <th>{{ __('table.Email') }}</th>
                  <th>{{ __('table.Courier Code') }}</th>
                  <th>{{ __('table.Client Code') }}</th>
                  <th>{{ __('table.Phone') }}</th>
                  <th>{{ __('table.Status') }}</th>
                  <th>{{ __('table.Created Date') }}</th>
                  <td class="no-sort" >{{ __('table.action.action') }}</td>
              </tr>
              </thead>
              <tfoot>
              <tr>
              <th rowspan="1" colspan="1"></th>
              <th rowspan="1" colspan="1"></th>
              <th rowspan="1" colspan="1"></th>
              <th rowspan="1" colspan="1"></th>
              <th rowspan="1" colspan="1"></th>
              <th rowspan="1" colspan="1"></th>
              <th rowspan="1" colspan="1"></th>
              <th rowspan="1" colspan="1"></th>
              </tr>
              </tfoot>
              <tbody>
                 <?php $i=0; ?>    @foreach($userData as $data)
              <tr class="gradeX">
                  <td>{{ ++$i  }}</td>
                  <td>{{ $data->email }}</td>
                  <td>{{ $data->courier_code }}</td>
                  <td>{{ $data->client_code }}</td>
                  <td>{{ $data->phone }}</td>
                  <td>{{ $data->status }}</td>
                  <td>{{ $data->created_date }}</td>
                  <td><a class="btn btn-danger btn-xs" onclick="return confirm('Are you sure delete this?')" href="{{ route('submit.action',['id'=>$data->id,'type'=>'delete','db'=>'savedetails']) }}"><i class="fa fa-trash-o "></i></a></td>        
              </tr>
              @endforeach
              </tbody>
              </table>
              </div>
              </div>
              </section>
              </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!-- Right Slidebar start -->
      
@endsection
@section('js')
<script>
var oTable = $('#tsubmit').dataTable({
        "order": [],
        "columnDefs": [{
        "targets"  : 'no-sort',
        "orderable": false,
        }],
        "pageLength": 50,
        //"bStateSave": true,
        "fnInitComplete": function () {
            this.api().columns([1,2,3,4,5]).every( function () {
                var column = this;
                var select = $('<select style="width:100%;"><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    var val = $.fn.dataTable.util.escapeRegex(d);
                    if (column.search() === "^" + val + "$") {
                    select.append(
                        '<option value="' + d + '" selected="selected">' + d + "</option>"
                    );
                    } else {
                    select.append('<option value="' + d + '">' + d + "</option>");
                    }
                                } );
                            } );
        }
    });
    
        
function submitCourier(){
    let form = 'form_couirer';
    let popup = 'addcourier';
    let courierCode = $('#'+form).find('input[name=courier_code]');
    let userId = $('#'+form).find('input[name=userId]');
    let formData = new FormData();
    //console.log(courierCode);
    formData.append('courier_code',courierCode.val());
    formData.append('user_id',userId.val());
    $.ajax({
        url: '/api/savedetails_new',
        type : 'POST',
        data : formData,
        processData : false,
        contentType : false,
        success : function(data) {
            console.log(data);
            if (typeof data.response !== 'undefined') 
            {
                if (data.response == false) 
                {
                    courierCode.val('');
                    $('#'+popup).modal('hide');
                    alert(data.message);
                    return;                        
                }
            }               
            courierCode.val('');
            $('#'+popup).modal('hide');
            alert(data.message);
            location.reload(true);
        }
    });
}
</script>
@endsection
      
     
