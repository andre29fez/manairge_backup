<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
  
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Scanner Admin</title>

    <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-reset.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/font-awesome/css/font-awesome.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}">
   
        <link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/slidebars.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/style-responsive.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/advanced-datatable/media/css/demo_page.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/advanced-datatable/media/css/demo_table.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/data-tables/DT_bootstrap.css') }}">


          <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap-datepicker/css/datepicker.css') }}">
          <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap-timepicker/compiled/timepicker.css') }}">
          <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap-daterangepicker/daterangepicker-bs3.css') }}">
          <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap-datetimepicker/css/datetimepicker.css') }}">
          <link rel="stylesheet" href="{{ URL::asset('assets/colorpicker/css/bootstrap-colorpicker.min.css') }}">
          <link rel="stylesheet" href="{{ URL::asset('assets/toastr-master/toastr.css') }}">
          @yield('css')
  </head>

  <body class="light-sidebar-nav">

  <section id="container">
      <!--header start-->
      <header class="header white-bg">
              <div class="sidebar-toggle-box">
                  <i class="fa fa-bars"></i>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo">Scanner<span>Admin</span></a>
            <!--logo end-->
           
            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <li>
                        <input type="text" class="form-control search" placeholder="Search">
                    </li>
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" src="{{ url('img/avatar1_small.jpg') }}">
                            <span class="username">{{ ucfirst(Auth::user()->name) }}</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout dropdown-menu-right">
                            <div class="log-arrow-up"></div>
                            <li><a href="{{ url('/ProfileSetting') }}"><i class=" fa fa-suitcase"></i>{{ trans('menu.profile') }}</a></li>
                            <li><a href="{{ url(Session::get('locale')!='cn'? '/lang/cn':'/lang/en') }}"><i class=" fa fa-globe"></i>{{ Session::get('locale')!= 'cn' ? 'CN' : 'EN' }}</a></li>
                            <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-key"></i> 
                                        {{ __('menu.log_out') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                        </ul>
                    </li>
                    <li class="sb-toggle-right">
                        <i class="fa  fa-align-right"></i>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>
@include('layout.sidebar')
@yield('content')
@include('layout.footscript')
@yield('js')
</body>
</html>