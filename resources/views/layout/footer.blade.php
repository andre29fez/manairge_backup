<footer class="site-footer">
          <div class="text-center">
              2020 &copy; Clarigoinfotech.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
       <script src="{{ URL::asset('js/jquery.js') }}"></script>
       <script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
       <script src="{{ URL::asset('js/jquery.dcjqaccordion.2.7.js') }}"></script>
       <script src="{{ URL::asset('js/jquery.scrollTo.min.js') }}"></script>
       <script src="{{ URL::asset('js/jquery.nicescroll.js') }}"></script>
       <script src="{{ URL::asset('js/jquery.sparkline.js') }}"></script>
       <script src="{{ URL::asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script>
       <script src="{{ URL::asset('js/owl.carousel.js') }}"></script>
       <script src="{{ URL::asset('js/jquery.customSelect.min.js') }}"></script>
       <script src="{{ URL::asset('js/respond.min.js') }}"></script>
       <script src="{{ URL::asset('js/slidebars.min.js') }}"></script>
       <script src="{{ URL::asset('js/common-scripts.js') }}"></script>
       <script src="{{ URL::asset('js/sparkline-chart.js') }}"></script>
       <script src="{{ URL::asset('js/easy-pie-chart.js') }}"></script>
       <script src="{{ URL::asset('js/count.js') }}"></script>
       <script src="{{ URL::asset('assets/advanced-datatable/examples/examples_support/jquery.jeditable.js')}}"></script>
       <script src="{{ URL::asset('assets/advanced-datatable/media/js/jquery.dataTables.js') }}"></script>
       <script src="{{ URL::asset('assets/data-tables/DT_bootstrap.js') }}"></script>
       <script src="{{ URL::asset('js/dynamic_table_init.js') }}"></script>


   <script src="{{ URL::asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
   <script src="{{ URL::asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
   <script src="{{ URL::asset('assets/bootstrap-daterangepicker/moment.min.js') }}"></script>
   <script src="{{ URL::asset('assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
   <script src="{{ URL::asset('assets/colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
   <script src="{{ URL::asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

  <script src="{{ URL::asset('js/pickers/init-date-picker.js') }}"></script>
  <script src="{{ URL::asset('js/pickers/init-datetime-picker.js') }}"></script>
  <script src="{{ URL::asset('js/pickers/init-color-picker.js') }}"></script>
 
  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

      $(window).on("resize",function(){
          var owl = $("#owl-demo").data("owlCarousel");
          owl.reinit();
      });

  </script>
   
  </body>
</html>