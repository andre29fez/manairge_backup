 <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
             
              <ul class="sidebar-menu" id="nav-accordion">
              @if(Auth::user()->authorizeRoles('admin'))
                  <li>
                      <a <?= $menu == 'dashboard' ? ' class="active"' : ''?> href="{{ url('dashboard') }}">
                          <i class="fa fa-dashboard"></i>
                          <span>@lang('menu.dashboard')</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" <?= $menu == __('menu.product') ? ' class="active"': ''?>>
                          <i class="fa fa-shopping-cart"></i>
                          <span>@lang('menu.product')</span>
                      </a>
                      <ul class="sub">
                          <li <?= isset($sub_menu) && $sub_menu == 'addcsv' ? ' class="active"' : ''?>><a  href="{{ url('admin/AddCsv') }}">@lang('menu.add_csv_file')</a></li>
                          <li <?= isset($sub_menu) && $sub_menu == 'Productlist' ? ' class="active"' : ''?>><a  href="{{ url('admin/ProductList') }}">@lang('menu.product_list')</a></li>
                        
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" <?= $menu == __('menu.news') ? ' class="active"': ''?>>
                          <i class="fa fa-archive"></i>
                          <span>@lang('menu.news')</span>
                      </a>
                      <ul class="sub">
                          <li <?= isset($sub_menu) && $sub_menu == 'Articles' ? ' class="active"' : ''?>><a  href="{{ url('admin/news') }}">@lang('menu.articles')</a></li>
                          <li <?= isset($sub_menu) && $sub_menu == 'Category' ? ' class="active"' : ''?>><a  href="{{ url('admin/category') }}">@lang('menu.category')</a></li>
                        
                      </ul>
                  </li>
                    @endif
                    <li class="sub-menu"  >
                    <a href="javascript:;" <?= $menu == __('menu.user_submit') ? ' class="active"': ''?>>
                    <i class="fa fa-list"></i>
                    <span>@lang('menu.user_submit')</span>
                    </a>
                    <ul class="sub ">
                    <li <?= isset($sub_menu) && $sub_menu == 'details' ? ' class="active"' : ''?>><a  href="{{ url('user/submit') }}">@lang('menu.detail_list')</a></li>

                    </ul>
                    </li>

                   <li class="sub-menu"  >
                      <a href="javascript:;" <?= $menu == __('menu.setting') ? ' class="active"': ''?>>
                          <i class="fa fa-cogs"></i>
                          <span>@lang('menu.setting')</span>
                      </a>
                      <ul class="sub ">
                          <li <?= isset($sub_menu) && $sub_menu == 'ProfileSetting' ? ' class="active"' : ''?>><a  href="{{ url('ProfileSetting') }}">@lang('menu.profile_setting')</a></li>
                          
                      </ul>
                       @if(Auth::user()->authorizeRoles('admin'))
                      <ul class="sub ">
                          <li <?= isset($sub_menu) && $sub_menu == 'users' ? ' class="active"' : ''?>><a  href="{{ url('Users') }}">@lang('menu.users')</a></li>
                      </ul>
                      <ul class="sub ">
                          <li <?= isset($sub_menu) && $sub_menu == 'backup' ? ' class="active"' : ''?>><a  href="{{ url('admin/backup') }}">@lang('menu.backup_restore')</a></li>
                      </ul>
                      @endif
                  </li>

               
                  <!--multi level menu end-->

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>