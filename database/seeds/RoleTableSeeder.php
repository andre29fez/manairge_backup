<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->name = 'user';
        $role_user->description = 'A Employee User';
        $role_user->save();    
        
        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'A Administrator User';
        $role_admin->save();

        $role_cs = new Role();
        $role_cs->name = 'customer';
        $role_cs->description = 'A Customer Service User';
        $role_cs->save();

        $role_inspector = new Role();
        $role_inspector->name = 'inspector';
        $role_inspector->description = 'A inspector User';
        $role_inspector->save();

        $role_agent = new Role();
        $role_agent->name = 'agent';
        $role_agent->description = 'A Agent User';
        $role_agent->save();
    }
}
