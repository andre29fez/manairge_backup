<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting();
        $setting->keyname = 'use_month_filter';
        $setting->keyvalue = '1'; //default
        $setting->keytype = 'number';
        $setting->save();
    }
}