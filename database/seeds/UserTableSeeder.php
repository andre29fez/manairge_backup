<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $role_customer = Role::where('name', 'customer')->first();
        $role_inspector = Role::where('name', 'inspector')->first();
        $role_agent = Role::where('name', 'agent')->first();

        $data = [
            ['name' => 'user', 'email' => 'user@user.com', 'password' => '123123', 'role' => $role_user],
            ['name' => 'admin', 'email' => 'admin@admin.com', 'password' => '123123', 'role' => $role_admin],
            ['name' => 'customer', 'email' => 'customer@customer.com', 'password' => '123123', 'role' => $role_customer],
            ['name' => 'inspector', 'email' => 'inspector@inspector.com', 'password' => '123123', 'role' => $role_inspector],
            ['name' => 'agent', 'email' => 'agent@agent.com', 'password' => '123123', 'role' => $role_agent]
        ];

        foreach($data as $key){
            $user = new User();
            $user->name = $key['name'];
            $user->email = $key['email'];
            $user->password = Hash::make($key['password']);
            $user->phone = '12345678';
            $user->profile_pic = 'img/profile-avatar.jpg';
            $user->remark = 'remark';
            $user->status = '1';
            $user->client_code = '';
            $user->save();
            $user->roles()->attach($key['role']);    
        }
       
        // $manager = new User();
        // $manager->name = ‘Manager Name’;
        // $manager->email = ‘manager@example.com’;
        // $manager->password = bcrypt(‘secret’);
        // $manager->save();
        // $manager->roles()->attach($role_manager);

    }
}
