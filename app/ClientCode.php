<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCode extends Model
{
    protected $table = 'client_codes';
    protected $fillable= ['client_code','updated_at'];
}
