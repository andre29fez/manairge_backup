<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'admin_login';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password','email','phone','client_code','client_code_s','profile_pic','status','role','remark'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     public static $login_validation_rule = [
        'username' => 'required|username|exists:admin_login',
        'password' => 'required',
        //'company' => 'required'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
   /**
    * * @param string|array $roles
    */
   public function authorizeRoles($roles){  
       if (is_array($roles)) {      
            return $this->hasAnyRole($roles) || false;  
        }  
        return $this->hasRole($roles) || false;
    }

    public function authoriz($roles){
        $redirectPath = 'user/submit';
        if (is_array($roles)) {      
             return $this->hasAnyRole($roles) || redirect($redirectPath);
         }  
         return $this->hasRole($roles) || redirect($redirectPath);
     }
    
    /**
     * * Check multiple roles
     * * @param array $roles
     * */
    public function hasAnyRole($roles){  
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    /**
     * * Check one role
     * * @param string $role
     * */
    public function hasRole($role){ 
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function getRoles($role){ 
        return null !== $this->roles()->whereNotIn('name', $role)->get();
    }
    
    public function activationUser()
    {
        return $this->hasOne('App\ActivationUser');
    }

}
