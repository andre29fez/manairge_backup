<?php

namespace App\Exports;
//use App\Models\ShopProduct;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Product;

class ProductExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents
{
    public function __construct($data,$type)
    {
        if($type=="spatie"){
            $this->tObj = $data;
        }else{
            $this->dx = explode('|',$data);
            $this->fdate = $this->dx[0] ?? '';
            $this->tdate = $this->dx[1] ?? '';
            $this->tstatus = $this->dx[2] ?? '';
            $this->tsort = $this->dx[3] ?? 'desc';
            $this->tclient = $this->dx[4] ?? '';
            $this->tObj = new Product;
            if($this->fdate!='' && $this->tdate!=''){
                $this->tObj = $this->tObj->whereRaw("modified_date >= ? AND modified_date <= ?", 
                array($this->fdate." 00:00:00", $this->tdate." 23:59:59"));
            }
            if($this->tstatus!=''){
                $this->tObj = $this->tObj->where('status',$this->tstatus);
            }
            if($this->tclient!=''){
                $this->tObj = $this->tObj->whereIn('client_code',explode(',',$this->tclient));
            }
            $this->tObj = $this->tObj->orderBy('modified_date',$this->tsort);
        }
       

    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         $prod = $this->tObj->select(
            'container_code',
            'modified_date',
            'item_remark',
            'discount',
            'courier_code',//must
            'courier_symbol',
            'quantity',
            'rmb',
            'rm',
            'weight',
            'size',//must
            'courier_fee',
            'amount',
            'remark',
            'arrival_date',
            'client_code',
            'status'
            )->get();
       // $prod = $this->tObj;
        return $prod;    
    }
    public function headings(): array
    {
        return [
            'container_code',
            'time',
            'item_remark',
            'discount',
            'courier_code',//must
            'courier_symbol',
            'quantity',
            'rmb',
            'rm',
            'weight',
            'size',//must
            'courier_fee',
            'amount',
            'remark',
            'arrival_date',
            'client_code',
            'status'
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:Q1'; // All headers
                $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        ]
                        ,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],
                ];
                $event->sheet->getDelegate()->getRowDimension('1')->setRowHeight(25);
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
                $event->sheet->getDelegate()->getStyle('E1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFF0000');
                $event->sheet->getDelegate()->getStyle('K1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFF0000');
                // $event->sheet->getDelegate()->getStyle('D2:D1000')->getNumberFormat()
                // ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
                $spreadsheet = $event->sheet->getParent()->getProperties()
                ->setCreator('Ismizhu')
                ->setLastModifiedBy('Yuwono')
                ->setTitle('Product Export')
                ->setSubject('Scanner Admin Product Export')
                ->setDescription(
                'Generated document for Office 2010 XLSX'
                )
                ->setKeywords('pintarmedia pintarapp ismizhuone@gmail.com')
                ->setCategory('excel document');
            },
        ];
    }
}
