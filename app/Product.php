<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'product';
    const UPDATED_AT = 'modified_date';
    const CREATED_AT = 'created_date';

    protected $fillable = [
        'container_code', 'Time', 'item_remark', 'courier_code', 'courier_symbol', 'quantity', 'rmb', 'rm', 'size', 'courier_fee', 'amount', 'remark', 'arrival_date', 'client_code', 'status', 'scan_time', 'discount', 'weight'
    ];
   // protected $appends = ['total']; //last 24 hours
  // public $total = 0;
    public function scopeStartsBetween(Builder $query, $from, $to): Builder
    {
        // $tObj = $tObj->whereRaw("modified_date >= ? AND modified_date <= ?", 
        // array($fdate." 00:00:00", $tdate." 23:59:59"));
        if($from!='' && $to !=''){
            return $query->whereBetween('modified_date',[$from." 00:00:00",$to." 23:59:59"]);
        }else{
            return $query;
        }
    }
   

}
