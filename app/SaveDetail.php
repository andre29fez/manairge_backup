<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveDetail extends Model
{
    protected $table = 'savedetails';
    protected $fillable = ['courier_code', 'client_code', 'phone', 'email', 'status'];
    
}
