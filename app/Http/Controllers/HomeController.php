<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ActivationUser;
use App\Product;    
//use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function test($cc)
    {
        //$test = gethelper('data');
        $courier_code = $cc;
        $product = Product::withTrashed()->where('courier_code', $courier_code);//->trashed()
        if($product->first()->trashed()){
         //$details1['status'] =   'Shipping';
           //$product->update($details1);
            return 'trash';
        }else{
            return 'no';
        }
        //return $check;
    }
    public function Activation($token)
    {
        $verificationUser = ActivationUser::where('token', $token)->first();
        if(isset($verificationUser) ){
            $user = $verificationUser->user;
            if(!$user->status) {
                $verificationUser->user->status = 1;
                $verificationUser->user->save();
                $status = "Your Email Has Been Verified. Please login now.";
            }else{
                $status = "Your Email Has Been Verified Previously. Please login now.";
            }
        }else{
            return redirect('/')->with('message', "Sorry, the email could not be identified.");
        }
 
        return redirect('/')->with('message', $status);
    }
}
