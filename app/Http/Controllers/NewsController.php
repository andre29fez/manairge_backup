<?php

namespace App\Http\Controllers;

use App\News;
use App\Category;
use Illuminate\Http\Request;
use VideoThumbnail;

class NewsController extends Controller
{

    public function index()
    {
        $data['menu'] = __('menu.news');
        $data['sub_menu'] = 'Articles';
        $data['newslist'] = News::with('category')->latest()->get();
        return view('admin.news.index', $data);
    }


    public function create()
    {
        $data['menu'] = __('menu.news');
        $data['sub_menu'] = 'Articles';
        $data['categories'] = Category::latest()->select('id','name')->where('status',1)->get();
        return view('admin.news.create', $data);
    }


    public function store(Request $request)
    {
        $request->validate([
            'title'         => 'required|unique:news|max:255',
            'details'       => 'required',
            'image'         => 'image|mimes:jpg,png,jpeg',
            'video'         => 'mimes:mp4,mov,ogg,webm|max:100000',
        ]);
    
        $status = true; //publish
        $featured = true; //on
        $imageName = 'no-thumbnail-post.jpg';
        $videoThumb = 'no-video.png';
        if ($request->hasFile('image')) {
            $imageName = 'news-'.time().uniqid().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('img/news'), $imageName);
        }
        if ($request->hasFile('video')) {
            $video = 'video-'.time().uniqid().'.'.$request->video->getClientOriginalExtension();
            $request->video->move(public_path('img/news/video'), $video);
            $thumbnail_path= public_path('img/news/vthumbs');
            $videoUrl  = public_path('img/news/video/').$video;
            //$video_path       = $destination_path.'/'.$file_name;
            $thumbnail_image  = $video.".jpg";
            $thumbnail_status = VideoThumbnail::createThumbnail($videoUrl,$thumbnail_path,$thumbnail_image,2,$width = 640, $height = 480);
            //VideoThumbnail::createThumbnail($videoUrl, $storageUrl, $fileName, $second, $width = 640, $height = 480);
            //$abc = VideoThumbnail::createThumbnail(public_path('img/news/'), public_path('files/thumbs/'), 'movie.jpg', 2, 1920, 1080);
            if($thumbnail_status)
            {
                $videoThumb = $thumbnail_image;
              
            }
          
        }
            News::create([
                'title'         => $request->title,
                'slug'          => str_slug($request->title),
                'details'       => $request->details,
                'category_id'   => $request->category_id ?? 1,
                'image'         => $imageName,
                'video'         => $videoThumb,
                'status'        => $status,
                'featured'      => $featured
            ]);
    
        return redirect()->route('news.index')->with(['message' => 'News created successfully!']);
    }


    public function show(News $news)
    {
        //
    }


    public function edit(News $news)
    {   
        $data['menu'] = __('menu.Edit News');
        $data['sub_menu'] = 'Articles';
        $data['categories'] = Category::latest()->select('id','name')->where('status',1)->get();
        $data['news'] = News::findOrFail($news->id);     
        return view('admin.news.edit', $data);
    }

 
    public function update(Request $request, News $news)
    {
        $request->validate([
            'title'         => 'required|max:255',
            'details'       => 'required',
            'image'         => 'image|mimes:jpg,png,jpeg',
            'video'         => 'mimes:mp4,mov,ogg,webm|max:100000',
        ]);
        $status = true; //publish
        $featured = true; //on    
        $news = News::findOrFail($news->id);

        if ($request->hasFile('image')) {
            if(file_exists(public_path('img/news') . $news->image)){
                unlink(public_path('img/news') . $news->image);
            }

            $imageName = 'news-'.time().uniqid().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('img/news'), $imageName);

        }else{
            $imageName = $news->image;
        }

        if ($request->hasFile('video')) {
            if(file_exists(public_path('img/news/video') . $news->video)){
                unlink(public_path('img/news/video') . $news->video);
            }

            $video = 'news-'.time().uniqid().'.'.$request->video->getClientOriginalExtension();
            $request->video->move(public_path('img/news/video'), $video);
            $thumbnail_path= public_path('img/news/vthumbs');
            $videoUrl  = public_path('img/news/video/').$video;

            $thumbnail_image  = $video.".jpg";
            $thumbnail_status = VideoThumbnail::createThumbnail($videoUrl,$thumbnail_path,$thumbnail_image,2,$width = 640, $height = 480);
        
            if($thumbnail_status)
            {
                $videoThumb = $thumbnail_image;
              
            }

        }else{
            $videoThumb = $news->video;
        }

        $news->update([
            'title'         => $request->title,
            'slug'          => str_slug($request->title),
            'details'       => $request->details,
            'category_id'   => $request->category_id ?? 1,
            'image'         => $imageName ?? 'no-thumbnail-post.jpg',
            'video'         => $videoThumb ?? 'no-video.png',
            'status'        => $status,
            'featured'      => $featured
        ]);

        return redirect()->route('news.index')->with(['message' => 'News updated successfully!']);
    }

 
    public function destroy(News $news)
    {
        $news = News::findOrFail($news->id);
        if($news->image !='no-thumbnail-post.jpg'){
            if(file_exists(public_path('img/news/') . $news->image)){
                unlink(public_path('img/news/') . $news->image);
            }
        }
        if($news->video !='no-video.png'){
            if(file_exists(public_path('img/news/vthumbs/') . $news->video)){
                unlink(public_path('img/news/vthumbs/') . $news->video);
            }
        }
        $videoname = str_replace('.jpg','',$news->video);
        if(file_exists(public_path('img/news/video/') . $videoname)){
            unlink(public_path('img/news/video/') . $videoname);
        }
        $news->delete();

        return back()->with(['message' => 'News deleted successfully!']);
    }
}
