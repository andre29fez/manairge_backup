<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
      public function authenticate(Request $request)
    {
        $this->validate($request, User::$login_validation_rule);
        $data = $request->only('email', 'password');
        if (Auth::attempt($data)) {
            $pref = \DB::table('preference')->get();
            
            if(!empty($pref)) {
                $prefData = AssColumn($a=$pref, $column='id');

                foreach ($prefData as $value) {
                    $prefer[$value->field] = $value->value;
                }
            }

            Session::put($prefer);
            
            $curr = \DB::table('currency')->where('id',Session::get('dflt_currency_id'))->first();
            
            $currency['currency_name'] = $curr->name;
            $currency['currency_symbol'] = $curr->symbol;
            Session::put($currency);
            //Session::get('dflt_lang')
            
            return redirect()->intended('dashboard');
        }

        return back()->withInput()->withErrors(['email' => "Invalid Username & Password"]);
    }

     public function logout()
    {
    	Auth::logout();
        \Session::flush();

    	return redirect('/login');
    }
}
