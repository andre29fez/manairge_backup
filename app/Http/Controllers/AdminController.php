<?php

namespace App\Http\Controllers;


use App\User;
use App\Role;
use App\Product;
use App\Setting;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\ActivationUser;
use DB;
use Session;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
//use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Exports\ProductExport;
use App\Imports\ProductImport;
use Illuminate\Support\Arr;
use Debugbar;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changeLang($lang = 'en')
    {
        \Session::put('locale', $lang);
        $locale = \Session::get('locale');
         \App::setLocale($locale);
         $locale2 = \App::getLocale();
         return redirect()->back();
    }
     public function dashboard(Request $request)
    {
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }
        if(!Session::get('id')){ return redirect('/'); }
        $data['menu'] = 'dashboard';
        $data['result'] = array(
            'tot_user' => User::all()->count(),
            'tot_product' => DB::table('product')->count(),
            'tot_news' => \App\News::all()->count(),
            'tot_usersubmit' => DB::table('savedetails')->count()     
        );
       // return $data['result'];
        return view('admin/dashboard',$data);
        
    }

public function ProfileSetting($ids= null)
    {   
        $data['menu'] = __('menu.setting');
        $data['sub_menu'] = 'ProfileSetting';
        $data['users'] = DB::table('admin_login')->where('status',1)->select('id','name','email')->get();
        if($ids != '' || $ids != null){
            Auth::user()->authorizeRoles(['admin']);
            $data['userData'] = DB::table('admin_login')->where('id', $ids)->first();
            if(!$data['userData']){
                $data['userData'] = DB::table('admin_login')->where('id', Session::get('id'))->first();
                \Session::flash('message',trans('notice.Data not found'));   
            }
        }else{
            if(!Session::get('id')){ return redirect('/'); }
            $data['userData'] = User::find(Auth::id());
        }
        return view('admin/ProfileSetting',$data);
   
    }


      public function updateprofile(Request $request, $id)
    {
        if(!Session::get('id')){ return redirect('/'); }
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $data['name'] = $request->name;
        $data['phone'] = $request->mobile;
        $data['remark'] = $request->remarks;
        $data['email'] = $request->email;
        $data['client_code'] = $request->client_code;
       

        $pic = $request->file('picture');

        if (isset($pic)) {
          $upload = 'img';

          $pic1 = $request->pic;
          if ($pic1 != NULL) {
            $dir = public_path("img/$pic1");
            if (file_exists($dir)) {
               unlink($dir);  
            }
          }

          $filename = $pic->getClientOriginalName();  
          $pic = $pic->move($upload, $filename);
          $data['profile_pic'] = $filename;
        }
        
        DB::table('admin_login')->where('id', $id)->update($data);
       
       // $data3['ip_address'] =  $request->ip();
      
        \Session::flash('message',trans('notice.Update Successfully'));
        
       
           return redirect()->intended("ProfileSetting"); 
        
    }


public function changepassword(Request $request, $id)
    {
        if(!Session::get('id')){ return redirect('/'); }
         $this->validate($request, [
            'old_pass' => 'required',
            'new_pass' => 'required',
        ]);
        $v = DB::table('admin_login')->where('id', '=', $id)->first();
        $data['password'] = $request->new_pass;  
        if ($request->old_pass==$v->password) {
            DB::table('admin_login')->where('admin_id', $id)->update($data);
            \Session::flash('message',__('notice.Password Update successfully'));
                return redirect()->intended("ProfileSetting");
        } else {
 \Session::flash('message',__('notice.Old Password is Wrong'));
            return back()->withInput()->withErrors(['message' => __('notice.Old Password is Wrong')]);
        }

    }


    public function ProductList(Request $request)
    {
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }
        if(!Session::get('id')){ return redirect('/'); }
        $isMonthFilter = $this->getSetting('use_month_filter') == '1' ? true : false;
        $c = $request->get('d');
        $data['is_trash'] = false;
        $data['sub_menu'] = 'Productlist';
        
        $data['client_codes']  = Product::select('client_code')->where('client_code','!=','')
        ->groupBy('client_code')->orderBy('client_code', 'asc')->get();
       
        if($c=='cd'){
            return redirect('admin/ProductList')->with('cd','dup');
        }else if($c=='rb'){
            return redirect('admin/ProductList')->with('cd','trash');
        }else{
        $dv = Session::get('cd');
        if($dv!=''){
            if($dv=='dup'){
            $check1 = Product::whereIn('courier_code', function ($q){
                $q->select('courier_code')
                ->from('product')
                ->groupBy('courier_code')
                ->havingRaw('COUNT(*) > 1');})->orderBy('courier_code', 'asc')->get();
             if (count($check1) != 0 ){
                Session::flash('message','Found multiple courier code!');
                $data['product']  = $check1;
             }else{
                Session::flash('message','Duplicate courier code not found!');
                $data['product']  = Product::orderBy('modified_date', 'desc')->get();
             }
            }
            if($dv=='search'){
                $fdate = session('fdate') ?? '';
                $tdate = session('tdate') ?? '';
                $tstatus = session('tstatus') ?? '';
                $tsort = session('tsort') ?? 'desc';
                $tclient = session('tclient') ?? '';
                $tObj = new Product;
                if($fdate!='' && $tdate!=''){
                    $tObj = $tObj->whereRaw("modified_date >= ? AND modified_date <= ?", 
                    array($fdate." 00:00:00", $tdate." 23:59:59"));
                }
                if($tstatus!=''){
                    $tObj = $tObj->where('status',$tstatus);
                }
                if($tclient!=''){
                    $tObj = $tObj->whereIn('client_code',explode(',',$tclient));
                }
                $data['product'] = $tObj->orderBY('modified_date',$tsort)->get();

            }
            if($dv=='trash'){
                $data['product'] = Product::onlyTrashed()->orderBy('modified_date', 'desc')->get();
                $data['is_trash'] = true;
            }
           
        }else{
             if($isMonthFilter){   
                $data['product'] = Product::orderBy('modified_date', 'desc')
                ->where('created_date','>', date('Y-m-d', strtotime("-90 days")))->get();
                $data['limit_date'] = date('Y-m-d', strtotime("-90 days"));
             }else{
                $data['product'] = Product::orderBy('modified_date', 'desc')->get();
             }
            //  $data['product'] = QueryBuilder::for(Product::class)
            // ->defaultSort('-modified_date')
            // ->get();
        }
        
       // trans('menu.product');
        if($data['is_trash']){
            $tit = '<i class="fa fa-trash-o fa-2x"></i> '. trans('menu.trash_can');
        }else{
            $tit = trans('menu.product');
        }
        $data['menu'] = $tit;
        $data['isMonthFilter'] = $isMonthFilter;
        return view('admin/product/Product',$data);
        }
    }
    public function getSetting($keyname){
        $setting = Setting::where('keyname',$keyname)->first();
        return $setting->keyvalue;
    }
    public function backup()
    {
        $download = request('download') ?? '';
        if ($download) {
            $file = storage_path() . "/backups/" . $download;
            if (file_exists($file)) {
                $headers = array(
                    'Content-Type' => 'application/octet-stream',
                );
                return response()->download($file, '', $headers);
            }
        }
        $arrFiles = [];
        foreach (glob(storage_path() . "/backups/*.sql") as $file) {
            if (file_exists($file)) {
                $fileInfo         = [];
                $fileInfo['path'] = $file;
                $arr              = explode('/', $file);
                $fileInfo['name'] = end($arr);
                $fileInfo['size'] = number_format(filesize($file) / 1048576, 2) . 'MB';
                $fileInfo['time'] = date('Y-m-d H:i:s', filemtime($file));
                $arrFiles[]       = $fileInfo;
            }
        }
        rsort($arrFiles);
        //return $arrFiles;
        return view('admin/backup/index')->with(
            [
                "menu"    => trans('menu.setting'),
                "sub_menu" => "backup",
                "arrFiles" => $arrFiles
            ])->render();
    }
    public function processBackupFile()
    {
        $file     = request('file');
        $action   = request('action');
        $pathFull = storage_path() . "/backups/" . $file;
        $return   = ['error' => '', 'msg' => ''];
        if ($action === 'remove') {
            try {
                unlink($pathFull);
                $return = ['error' => 0, 'msg' => trans('notice.remove_success')];
            } catch (\Exception $e) {
                $return = ['error' => 1, 'msg' => $e->getMessage()];
            }
        } else if ($action === 'restore') {
            try {
                // DB::transaction(function () use ($pathFull) {
                DB::unprepared(file_get_contents($pathFull));
                $return = ['error' => 0, 'msg' => trans('notice.restore_success')];
                // });
            } catch (\Exception $e) {
                Session::flash('message',$e->getMessage());
                $return = ['error' => 1, 'msg' => $e->getMessage()];
            }
        }

        return json_encode($return);
    }

    public function generateBackup()
    {
        $return = shell_exec("php " . base_path() . "/artisan BackupDatabase");
        return $return;
    }
    public function AddCsv()
    {
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }
        if(!Session::get('id')){ return redirect('/'); }
        $data['menu'] = __('menu.product');
        $data['sub_menu'] = 'addcsv';
      
        return view('admin/product/csv_view',$data);
   
    }

     public function uploadcsv(Request $request){
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }
    if(!Session::get('id')){ return redirect('/'); }
    if ($request->input('submit') != null ){
      $file = $request->file('file');
      // File Details 
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $tempPath = $file->getRealPath();
      $fileSize = $file->getSize();
      $mimeType = $file->getMimeType();
      // Valid File Extensions
      $valid_extension = array("csv");
      // 2MB in Bytes
      $maxFileSize = 2097152; 
      // Check file extension
      if(in_array(strtolower($extension),$valid_extension)){
        // Check file size
        if($fileSize <= $maxFileSize){
          // File upload location
          $location = 'img/csv';
          //$location1 = 'public/img/csv';
          // Upload file
          $file->move($location,$filename);
          // Import CSV to Database
          $filepath = public_path($location."/".$filename);
          // Reading file
          $file = fopen($filepath,"r");
          $importData_arr = array();
          $i = 0;
          while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
             $num = count($filedata );   
             // Skip first row (Remove below comment if you want to skip the first row)
             if($i == 0){
                $i++;
                continue; 
             }
             for ($c=0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata [$c];
             }
             $i++;
          }
          fclose($file);  
          // Insert to MySQL database
          $v = User::find(Auth::user()->id);
        //   if(!empty($importData[3])){
        //     $dataCompare = ['courier_code'=>$importData[3]];
        //     $insertData = array(
        //     "container_code"=>$importData[0],
        //     "Time"=>$importData[1],
        //     "item_remark"=>$importData[2],
        //     "courier_code"=>$importData[3],
        //     "courier_symbol"=>$importData[4],
        //     "quantity"=>$importData[5],
        //     "rmb"=>$importData[6],
        //     "rm"=>$importData[7],
        //     "size"=>$importData[8],
        //     "courier_fee"=>$importData[9],
        //     "amount"=>$importData[10],
        //     "remark"=>$importData[11],
        //     "arrival_date"=>$importData[12],
        //     "client_code"=>$importData[13]              
        //     );
         
        //     $dbsaveDet = [
        //         'email' => $v->email,
        //         'phone' => $v->phone,
        //         'courier_code' => $importData[3],
        //         'client_code' => $importData[13],
        //         'status' => 'Ordering'
        //     ];
        //     DB::table('product')->updateOrInsert($dataCompare,$insertData);
        //     DB::table('savedetails')->updateOrInsert($dataCompare,$dbsaveDet);
        //    Session::flash('message','Import Successful.');
         // return $importData_arr;
          foreach($importData_arr as $importData){
            if(!empty($importData[16])){
                $dataCompare = ['courier_code'=>$importData[16]];
                $insertData = array(
                "container_code"=>$importData[0],
                "Time"=>$importData[1] ?? Carbon::now(),
                "scan_time"=>$importData[2],
                //"item_remark"=>$importData[3],
                "discount"=>$importData[4],
                "client_code"=>$importData[5],
                "courier_symbol"=>$importData[6],
                "quantity"=>$importData[7],
                "rmb"=>$importData[8],
                "rm"=>$importData[9],
                "weight"=>$importData[10],
                "size"=>$importData[11],
                "courier_fee"=>$importData[12],
                "amount"=>$importData[13],
                "remark"=>$importData[14],
                "arrival_date"=>$importData[15],
                "courier_code"=>$importData[16]             
                );
                if(Product::where('courier_code', $importData[16])->where('item_remark',null)->exists()){
                    $insertData = Arr::prepend($insertData, $importData[3], 'item_remark');
                }

                if(Product::where('courier_code',$importData[16])->count()==0){
                    $insertData = Arr::prepend($insertData, $importData[3], 'item_remark');
                }
                $dbsaveDet = [
                    'email' => $v->email,
                    'phone' => $v->phone,
                    'courier_code' => $importData[16],
                    'client_code' => $importData[5],
                    'status' => 'Ordering'
                ];
                //return dd($dbsaveDet);
               DB::table('product')->updateOrInsert($dataCompare,$insertData);
                DB::table('savedetails')->updateOrInsert($dataCompare,$dbsaveDet);
               // DB::table('product')->insert($insertData);
            //User::insertData($insertData);
               Session::flash('message','Import Successful.');
            }else{
            Session::flash('message','Some Data Not Uploaded.Courier code and Size is required.');
            }
         }         
        }else{
          Session::flash('message','File too large. File must be less than 2MB.');
        }

      }else{
         Session::flash('message','Invalid File Extension.');
      }

    }
     //return $importData_arr;
     return redirect()->intended("admin/AddCsv"); 
    // Redirect to index
    
  }

    public function onchangestatus(Request $request, $id)
    {
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }
        if(!Session::get('id')){ return redirect('/'); }
         $this->validate($request, [
            'status' => 'required'
            
        ]);
        $data['status'] = $request->status;
        if($request->status == 'Arrival'){
            $data['arrival_date'] = Carbon::now();    
        }
            $x = DB::table('product')->where('id', $id)->update($data);
            return response()->json(['code' => 0 , 'message' => 'Status Change successfully', 'data'=> $x]); 
            //\Session::flash('message','Status Change successfully !');
             //   return redirect()->intended("admin/ProductList");
    }

     public function editproduct($id)
    {
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }
        if(!Session::get('id')){ return redirect('/'); }
           $data['menu'] = 'Product';
        $data['sub_menu'] = 'Productlist';
         $data['data'] = DB::table('product')->where('id', $id)->first();

        return view('admin/product/editproduct', $data);

      
    }
    public function getFilter(Request $request){

       // return $request->all();
        $type = $request->action;
        $prod = QueryBuilder::for(Product::class)
            ->defaultSort('-modified_date')
            ->allowedFilters([AllowedFilter::scope('starts_between'),'status','client_code']);
        $isMonthFilter = $this->getSetting('use_month_filter') == '1' ? true : false;
        $data['isMonthFilter'] = $isMonthFilter;
       // return $type;
        if($type =="search"){
            $data['is_trash'] = false;
            $data['sub_menu'] = 'Productlist';
            $data['client_codes']  = Product::select('client_code')->where('client_code','!=','')
            ->groupBy('client_code')->orderBy('client_code', 'asc')->get();
            if($data['is_trash']){
                $tit = '<i class="fa fa-trash-o fa-2x"></i> '. trans('menu.trash_can');
            }else{
                $tit = trans('menu.product');
            }
            $data['menu'] = $tit;
            $data['product'] = $prod->get();
            //$data['timer'] = Debugbar::addMeasure('now', LARAVEL_START, microtime(true));
            return view('admin/product/Product',$data);
        }
        elseif($type =="excel"){
            $fn = $type.'-'.date('Y-m-d_H-i-s');
            return Excel::download(new ProductExport($prod,"spatie"), $fn.'_byTime.xlsx');
            //return 'exports';    
        
        }else{
            \Session::flash('message',trans('Oopss something wrong!'));
            return redirect()->back();
        }
        

    }
    public function excel(Request $request,$action,$type){
        //$type = $request->get('type');
        switch($action){
            case 'import':
                if($type=='product'){
                    $check = $this->validate($request, [
                        'upload_product' => 'required|mimes:csv,xls,xlsx'//
                    ]);
                    if(!$check){
                        return response()->json(['message' => 'required file type xlsx']);      
                    }
                    if ($request->hasFile('upload_product')) {
                        $file = $request->file('upload_product');
                    }
                    try {
                        Excel::import(new ProductImport(), $file);
                        return response()->json(['message' => 'Import Successful']);
                    }catch(\Maatwebsite\Excel\Validators\ValidationException $e) {
                        $failures = $e->failures();
                        foreach ($failures as $failure) {
                             $failure->row(); // row that went wrong
                             $failure->attribute(); // either heading key (if using heading row concern) or column index
                             $failure->errors(); // Actual error messages from Laravel validator
                             $failure->values(); // The values of the row that has failed.
                         }
                        return response()->json(['error' => $e->failures()]);
                    }
                }else{
                    \Session::flash('message',trans('Oopss something wrong!'));
                    return redirect()->back();
                }
            break;
            case 'export':
                // private function export($class, $filename, $type)
                // {
                //     if (! in_array($type, ['xls', 'csv'])) {
                //         $type = 'csv';
                //     }

                //     $fn = $filename.'-'.date('Y-m-d_H-i-s');

                //     return Excel::download(new $class, $fn.'.'.$type);
                // }
                if($type=='product'){
                    $fn = $type.'-'.date('Y-m-d_H-i-s');
                    $act = $request->get('by') ?? '';
                    $fdate = $request->get('fdate') ?? '';
                    $tdate = $request->get('tdate') ?? '';
                    $tstatus = $request->get('tstatus') ?? '';
                    $tsort = $request->get('tsort') ?? 'desc';
                    $tclient = $request->get('tclient') ?? '';
                    if($tclient!=''){
                        $tclient = implode(',',$tclient);
                    }
                    //return $tclient;
                    $arr = '';
                    if($act=='all'){
                        //$arr = '2020-04-01|2020-04-05|Arrival|desc';
                        return Excel::download(new ProductExport($arr,"no"), $fn.'.xlsx');     
                    }else if($act=='excel'){
                        $arr = $fdate.'|'.$tdate.'|'.$tstatus.'|'.$tsort.'|'.$tclient;
                        //return $arr;
                        return Excel::download(new ProductExport($arr,"no"), $fn.'_byTime.xlsx');

                    }else if($act=='select'){

                    }else{
                        \Session::flash('cd','search');
                        \Session::flash('fdate',$fdate);      
                        \Session::flash('tdate',$tdate);      
                        \Session::flash('tstatus',$tstatus);
                        \Session::flash('tsort',$tsort);
                        \Session::flash('tclient',$tclient);
                        return redirect('admin/ProductList');
                       // return response()->json(['code'=> 0, 'message' => 'search Successful', 'url'=> route('product.list')]);                 
                    }
                    
                }else{
                    \Session::flash('message',trans('Oopss something wrong!'));
                    return redirect()->back();
                    //return abort(401, 'This action is unauthorized.');
                }
            break;
            default:
            return abort(401, 'This action is unauthorized.');
        }
    }

 public function updateproduct(Request $request, $id)
    {
        $this->validate($request, [
            'container_code' => 'required',
           
        ]);

        $data['container_code'] = $request->container_code;
        $data['Time'] = $request->Time;
        $data['item_remark'] = $request->item_remark;
        $data['courier_code'] = $request->courier_code;
        $data['courier_symbol'] = $request->courier_symbol;
        $data['quantity'] = $request->quantity;
        $data['rmb'] = $request->rmb;
        $data['rm'] = $request->rm;
        $data['size'] = $request->size;
        $data['courier_fee'] = $request->courier_fee;
        $data['amount'] = $request->amount;
        $data['remark'] = $request->remark;
        $data['arrival_date'] = $request->arrival_date;
        $data['client_code'] = $request->client_code; 
        //return $data;
         DB::table('product')->where('id', $id)->update($data);
         \Session::flash('message',trans('Update successfully'));
             return redirect()->intended("admin/editproduct/".$id);
    }

    public function Users()
    {
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }    
        if(!Session::get('id')){ return redirect('/'); }
        $data['menu'] = __('menu.setting');
        $data['sub_menu'] = 'users';
        $data['userData'] = User::with('roles')->get();
        return view('admin/users',$data);
   
    }


    public function useronchangestatus(Request $request, $id)
    {
        if(!Session::get('id')){ return redirect('/'); }
         $this->validate($request, [
            'status' => 'required'  
        ]);
        $data['status'] = $request->status;
      
        DB::table('admin_login')->where('id', $id)->update($data);
        \Session::flash('message','Status Change successfully !');
        return redirect()->intended("Users");
       

    }
  public function updateusers($id)
    {
        if(!Session::get('id')){ return redirect('/'); }
        $data['menu'] = 'Profile';
        $data['sub_menu'] = 'users';
        $data['data'] = User::where('id',$id)->with('roles')->first();
        $data['roles'] = Role::all();
        //return $data;
        //$data['data'] = DB::table('admin_login')->where('id', $id)->first();
        return view('admin/editusers', $data);  
    }

     public function updateuser(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'phone' => 'required|string|max:13',
            //'password' => 'required|string|min:6',
            'remark' => 'required|string',
            'role' => 'required',
            'client_code' => 'required',
        ]);
        $data['name'] = $request->name;    
        $data['phone'] = $request->phone;
        //$data['password'] = Hash::make($request->password);
        $data['remark'] = $request->remark;
        $data['client_code'] = $request->client_code;
        $data['client_code_s'] = $request->client_code_s;
        $roles = $request->role;
        $role_user = Role::where('name', $roles)->first();
        $item = User::where('id', $id)->firstOrFail();
        $item->roles()->where('user_id',$id)->update(['role_id'=>$role_user->id]);
        $item = $item->update($data);
        
        //$user = User::with('roles')->where('id', $id)->update($data);
       // return dd($item);
        //$user->roles()->attach($role_user);
        //DB::table('admin_login')->where('id', $id)->update($data);
        \Session::flash('message',trans('Update successfully'));
            return redirect()->intended("editusers/".$id);
    }

public function addusers()
    {
        if(!Session::get('id')){ return redirect('/'); }

            $data['menu'] = 'Profile';
            $data['sub_menu'] = 'users';
            $data['roles'] = Role::all('id','name');

            //return $roles; 
        return view('admin/addusers', $data);

      
    }


 public function adduser(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:admin_login|email',
            'name' => 'required|string|max:50',
            'phone' => 'required|string|max:13',
            'password' => 'required|string|min:6',
            'remark' => 'required|string',
            'role' => 'required',
            'client_code' => 'required',
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['password'] = $request->password;
        $data['remark'] = $request->remark;
        $data['client_code'] = $request->client_code;
        $data['status'] = '1';
        $roles = $request->role;
        $role_user = Role::where('name', $roles)->first();
        $user = User::create($data);
        $user->roles()->attach($role_user);
        $token = str_random(40);
        $verificationUser = ActivationUser::create([
          'user_id' => $user->id,
          'token' => $token
        ]);
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("ziova.xenren@gmail.com", "Buy-manairge");
        $email->setSubject("Register confirmation");
        $email->addTo($data['email'], $data['name']);
        $email->addContent(
            'text/html', templateemail('<b>Buy-Manarge</b>',url('user/Activation', $user->activationUser->token),'Buy-Manarge',$data['name'],url('img/logo.jpg'))
        );
        //$email->setTemplateId("d-f80097cdce73409c9afe72028a363edb");
        $sendgrid = new \SendGrid(env('SENDGRID_API_KEY',null));
        try {
            $response = $sendgrid->send($email);
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        \Session::flash('message',trans('Add user successfully'));
        return redirect()->intended("addusers");
    }



    public function searchproduct(Request $request)
    {
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }
        if(!Session::get('id')){ return redirect('/'); }
         

         // $this->validate($request, [
         //    'fromdate' => 'required',
         //    'todate' => 'required'
         //   ]);

        
        $details['fromdate'] = $request->fromdate;
        $details['todate'] = $request->todate;
         $details['status'] = $request->status;

             Session::put($details);
              return redirect()->intended("admin/SearchProductList");
    }

     public function SearchProductList()
    {
        if(Auth::user()->authorizeRoles(['user'])){
            return redirect('user/submit');
        }
        if(!Session::get('id')){ return redirect('/'); }

        $data['menu'] = 'Product';
        $data['sub_menu'] = 'Productlist';

     
            $fromdate1=Session::get('fromdate');
            $todate1=Session::get('todate');
            $status=Session::get('status');
            if($todate1 && $fromdate1 && $status){
                 $fromdate=date('Y-m-d',strtotime($fromdate1));
 $todate=date('Y-m-d',strtotime($todate1));
                $data['product'] = DB::table('product')->where('status',$status)->whereBetween('modified_date', [$fromdate." 00:00:00",$todate." 23:59:59"])->get();

               
            }elseif($fromdate1 && $todate1){

                $fromdate=date('Y-m-d',strtotime($fromdate1));
 $todate=date('Y-m-d',strtotime($todate1));
                 $data['product'] = DB::table('product')->whereBetween('modified_date', [$fromdate." 00:00:00",$todate." 23:59:59"])->get();

               
            }else{
                 $data['product'] = DB::table('product')->where('status',$status)->get();
            }
 
       
        return view('admin/product/Product',$data);
   
    }
    public function Registerconfirmation($id)
    {

          $data['status'] = '1';
       
            DB::table('admin_login')->where('email', $id)->update($data);
            
            
   return view('verifyreg');
    }
    
    
      public function submitlist()
    {
        if(!Session::get('id')){ return redirect('/'); }
        $data['menu'] = __('menu.user_submit');
        $data['sub_menu'] = 'details';
        $obj = DB::table('savedetails');
        $status = session('status');
        if($status != ''){
            $obj = $obj->where('status',$status);
        }
        if(Auth::user()->authorizeRoles(['user'])){
            $email = Auth::user()->email;
            $data['userData'] = $obj->where('email',$email)->orderBy('updated_date', 'desc')->get();
        }else{
            $data['userData'] = $obj->orderBy('updated_date', 'desc')->get();
        }
        return view('user/userquery',$data);
   
    }
    public function submitaction(Request $request,$type= null,$db= null){
        $idx = $request->get('id') ?? '';
        switch($type){
            case 'delete':                
                if($db=='savedetails'){
                    $v = DB::table('savedetails')->where('id', $idx)->delete();
                }elseif($db=='product'){
                    $v = Product::where('id', $idx)->delete();
                }elseif($db=='admin_login'){
                    $v = User::with('roles')->where('id', $idx)->first();
                    $cpas= $request->get('password');
                    if($cpas!=''){
                        $credentials = array('email'  => Auth::user()->email,'password' => $cpas);  
                        if (Auth::attempt($credentials)) {
                            $v->delete();
                            $x = DB::table('role_user')->where('user_id', $idx)->delete();
                            return response()->json(['code'=>1,'confirm' => 'Delete admin account successful']);
                        }else{ 
                            return response()->json(['code'=>0,'confirm' => 'Wrong password']);
                        }
                    }else{
                        foreach($v->roles as $k){
                            $roles[] = $k->name;
                        }
                        if(in_array('admin',$roles)){
                            return response()->json(['code'=>1,'confirm' => $idx]);      
                        }else{   
                            $v->delete();
                            $x = DB::table('role_user')->where('user_id', $idx)->delete();
                            return response()->json(['code'=>0,'confirm' => 'Your action was successful']);      
                        }
                    }
                    
                }else{
                    return abort(401, 'This action is unauthorized.');
                }
                return redirect()->back()->with('message', 'Your action was successful');
                break;
            case 'permanent':
                Product::onlyTrashed()->where('id',$idx)->forceDelete();
                return redirect()->back()->with('message', 'Data removed from trash');
                break;
            case 'undo':
                Product::onlyTrashed()->where('id',$idx)->restore();
                return redirect()->back()->with('message', 'Data restored');
                break;
            case 'undoall':
                Product::onlyTrashed()->restore();
                return redirect()->back()->with('message', 'All data restored successfully');
                break;            
            default:
            return abort(401, 'This action is unauthorized.');
        }  
    }
    
    public function searchdetails(Request $request)
    {
        if(!Session::get('id')){ return redirect('/'); }
         $status = $request->status;
             //Session::put($details);
             \Session::flash('status',$status);        
        return redirect('user/submit');
    }

}
