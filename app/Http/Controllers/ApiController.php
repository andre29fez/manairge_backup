<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use App\Product;
use App\Setting;
use App\SaveDetail;
use App\ActivationUser;
use App\ClientCode;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Carbon\Carbon;
use DB;
use Session;
use PDF;
use Excel;
use Mail;


class ApiController extends Controller
{
  public function index()
    {
          return response()->json(['messsage' => trans('welcome api')]);
    }

public function updatedetails(Request $request)
    {
       $this->validate($request, [
            'courier_code' => 'required'
           
        ]);
        $courier_code = $request->courier_code;
        $details['client_code'] = $request->client_code;
          $details['status'] = 'Arrival';
        $data=  DB::select("select * from product where courier_code='$courier_code'");
          if(count($data)){
            DB::table('product')->where('courier_code', $courier_code)->update($details);
              return response()->json(['response' => trans('true'),'message'=>trans('Save Successfully')]);
            }else{
              return response()->json(['response' => trans('false'),'error' => trans('Worng Courier code')]);             
            } 
    }


     public function savedetails(Request $request)
    {
       $this->validate($request, [
            'courier_code' => 'required'
        ]);
        $courier_code = $request->courier_code;
        $details['client_code'] = $request->client_code;
        $details['courier_code'] =  $request->courier_code;
           $details['status'] = 'Arrival';
        $data=  DB::select("select * from product where courier_code='$courier_code'");     
        if(count($data)){
          DB::table('product')->where('courier_code', $courier_code)->update($details);
          return response()->json(['response' => trans('true'),'message'=>trans('Save Successfully')]);
        }else{
          DB::table('product')->insert($details);
          return response()->json(['response' => trans('true'),'message'=>trans('Save Successfully')]);
        }    
    }
    
     public function checkscannerdetails(Request $request)
    {
       $this->validate($request, [
            'courier_code' => 'required',
            'client_code' => 'required'
        ]);
        $courier_code = $request->courier_code;
        $client_code = $request->client_code;
       $details['status'] = 'Arrival';
       $details['client_code'] = $client_code;
        $data=  DB::table('product')->where('courier_code', $courier_code)->get();
       if(count($data)!=0){
            //DB::table('product')->where('courier_code', $courier_code)->update($details);
            return response()->json(['response' => trans('true'),'message'=>trans('Ready for update')]);
        }else{
          return response()->json(['response' => trans('false'),'error' => trans('Wrong Courier code')]);
      }
    }


 public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'  
        ]);
          $v = DB::table('admin_login')->where('email', '=', $request->username)
          ->where('status', '=', 1)->first();
        if($v){
        $credentials = array('email'  => $request->username,'password' => $request->password);  
        if (Auth::attempt($credentials)) {
            $curr = User::with('roles')->where('email',$request->username)->first()->toArray();   
            $role = $curr['roles'][0]['name'];
            $collection = collect($curr);
            $user = $collection->put('role', ucfirst($role));
            $user = $user->forget('roles');
            return response()->json(['response' => trans('true'),'data' => $user]);
       }else{  
         // return response()->json(['response' => trans('false'),'error' => trans('Invalid Username & Password')]);
         return response()->json(['response' => trans('false'),'error' => trans('Invalid Username & Password')]);
       }
        }else{
          return response()->json(['response' => trans('false'),'error' => trans('Your Acount Not Active')]);
        }
    }

  public function signup(Request $request){

        $this->validate($request, [
            'email' => 'required|unique:admin_login,email',
            'name'  => 'required',
            'password' => 'required',
            'phone' => 'required',
            'remark' => 'required'
          
        ]);
        $v = DB::table('admin_login')->where('email', '=', $request->email)->first();
        if($v){
            return response()->json(['response' => trans('false'),'message'=>trans('Already Registered Email')]);
        }else{
        
                $data['name'] = $request->name;
                $data['email'] = $request->email;
                $data['phone'] = $request->phone;
                $data['password'] = Hash::make($request->password);
                $data['remark'] = $request->remark;
                //$data['role'] = 'User';
                $data['client_code'] = '';
                $data['status'] = '0';
                $roles = 'user';
                $role_user = Role::where('name', $roles)->first();
                $user = User::create($data);
                $user->roles()->attach($role_user);
                $token = str_random(40);
                $verificationUser = ActivationUser::create([
                  'user_id' => $user->id,
                  'token' => $token
                ]);
                $email = new \SendGrid\Mail\Mail(); 
                $email->setFrom(env('MAIL_FROM_ADDRESS','ziova.xenren@gmail.com'), env('MAIL_FROM_NAME','Buy-Manairge'));
                $email->setSubject("Register confirmation");
                $email->addTo($data['email'], $data['name']);
                $email->addContent(
                    'text/html', templateemail('<b>'.env('MAIL_COMPANY','Buy-Manairge').'</b>',url('user/Activation', $user->activationUser->token),env('MAIL_COMPANY','Buy-Manairge'),$data['name'],url('img/logo.jpg'))
                );
                //$email->setTemplateId("d-f80097cdce73409c9afe72028a363edb");
                $sendgrid = new \SendGrid(env('SENDGRID_API_KEY',null));
                try {
                    $response = $sendgrid->send($email);
                    //print $response->statusCode() . "\n";
                    //print_r($response->headers());
                    //print $response->body() . "\n";
                } catch (Exception $e) {
                    echo 'Caught exception: '. $e->getMessage() ."\n";
                }
         return response()->json(['response' => trans('true'),'message'=>trans('User Registration successfully. Please check your email to confirmation')]);
  }
}
public function datalist(Request $request){
  $type = $request->get('type');
  switch($type){
    case 'client_code':
        $v = DB::table('product')->select('client_code')->where('client_code','!=','')->groupBy('client_code')->orderBy('client_code', 'asc')->get()->toArray();
    break;
    case 'courier_code':
      $v = DB::table('product')->select('courier_code')->orderBy('courier_code', 'asc')->get()->toArray();
    break;  
  }
 
  if($v){
    $collection = collect($v);
    $f = $collection->implode($type, ',');
    return response()->json(['response' => trans('true'),'message'=>$f]);   
  }else{
    return response()->json(['response' => trans('false'),'message'=>trans('no data code')]);     
  }
}
public function actionTable(Request $request){
  $datas = $request->get('ids');
  $t = $request->get('type');
  $db = $request->get('db');
  $trash = $request->get('trash') ?? '';
  switch($t){
    case 'delete':
      if($datas!=''){
        $d = explode(',',$datas);
        if($db=='product'){
          if($trash == 'true'){
             Product::whereIn('id', $d)->forceDelete(); //onlyTrashed()
             $dat ='yes trash';
          }else{
            Product::whereIn('id', $d)->delete();
            $dat ='no trash';
          }
        }else{
          DB::table($db)->whereIn('id', $d)->delete();
        }
        return response()->json(['response' => trans('true'),'message'=>'Delete Successfully','data'=>$dat]);  
      }else{
        return response()->json(['response' => trans('false'),'message'=>'Empty data']);  
      }
    break;
    case 'status':
      if($datas!=''){
        $d = explode(',',$datas);
        $s = $request->get('sts');
        DB::table('product')->whereIn('id', $d)->update(['status'=>$s]);
        return response()->json(['response' => trans('true'),'message'=>'Update Successfully']);  
      }else{
        return response()->json(['response' => trans('false'),'message'=>'Empty data']);  
      }
    break;
    case 'restore':
      if($datas!=''){
        $d = explode(',',$datas);
        Product::onlyTrashed()->whereIn('id', $d)->restore();
        return response()->json(['response' => trans('true'),'message'=>'Restore Successfully']);  
      }else{
        return response()->json(['response' => trans('false'),'message'=>'Empty data']);  
      }
    break;
  }
}
public function actionHistory(Request $request){
  $query = $this->getQuery($request);
  $d = $query->paginate($request->get('limit') ?? 10);

  // switch($acc){
  //   case 1:
  //     $d = Product::select('id','courier_code','client_code','item_remark','scan_time','size')
  //     ->orderBy('modified_date','desc')->paginate($limit);
  //   break;
  //   case 2:
  //     $user = User::find($user_id);
  //     $d = DB::table('savedetails')->where('email',$user->email)->select('id','courier_code','client_code','updated_date','status')->orderBy('updated_date','desc')->paginate($limit);
  //   break;
  // }
  $page = $request->get('page');
  return response()->json([
    'code' => (count($d) != 0) ? 1 : ($page != 1 ? 1 : 0),
    'message' => trans((count($d) != 0) ? 'success' : 'no history'),
    'data' => $d,
  ]); 
}

public function getQuery(Request $request){
  $limit = $request->get('limit');
  $acc = $request->get('acc');
  $userId = $request->get('userid');
  switch ($acc) {
    case 1:
      $query = Product::query();
      if ($request->has('hour')) {
        if($request->get('hour') != -1){
          $query->where('scan_time', '>', Carbon::parse('-'.$request->get('hour').' hours')->format('Y-m-d h:i:s'));
        }
      }
      if($request->get('hour') != -1){
        $query->select('id','courier_code','client_code','item_remark','scan_time','size',DB::raw('count(*) as total'));
        $query->groupBy('client_code');
      }else{
        $query->select('id','courier_code','client_code','item_remark','scan_time','size',DB::raw("0 as total"));
      }
      $query->orderBy('modified_date','desc');
     //$query->selectRaw('count(*) as total, client_code');
      break;
    case 2:
      $user = User::find($userId);
      $query = SaveDetail::query();
      $query->where(['email' => $user->email]);
      if ($request->has('hour')) {
        if($request->get('hour') != -1){
          $query->where('scan_time', '>', Carbon::parse('-'.$request->get('hour').' hours')->format('Y-m-d h:i:s'));
        }
      }
      $query->select('id','courier_code','client_code','updated_date','status');
      $query->orderBy('updated_date','desc');
      break;
  }
  return $query;
}
public function saveclient(Request $request){
  $this->validate($request, [
    'client_code' => ['required'],
  ]);
  $data = ClientCode::updateOrCreate(
    ['client_code' => $request->get('client_code')],
    ['updated_at' => date('Y-m-d H:i:s')]
  );
  //return response()->json(['code' => 0, 'response' => trans('true'),'message'=>$data->client_code]);   
  return $this->getClient($request);
  //return response()->json(['code' => 0,'message'=>'Show recently history','data' => $data]);     
}
public function getClient(Request $request){
  $query = ClientCode::query();
  $query->select('client_code');
  $query->orderBy('updated_at','desc');
  $query->take(20);
  $data = $query->get()->toArray();
  if($data){
    $collection = collect($data);
    $f = $collection->implode('client_code', ',');
    return response()->json(['code' => 0, 'response' => trans('true'),'message'=>$f]);   
  }else{
    return response()->json(['code' => 3, 'response' => trans('false'),'message'=>trans('no data code')]);     
  }
  // $data = $query->paginate($request->get('limit') ?? 20);
  // return response()->json(['code' => 0,'message'=>'Show recently history','data' => $data]);  
}
  
public function saveInput(Request $request){
  $this->validate($request, [
    'id' => 'required',
    'row_id' => 'required',
  ]);
  $idx = $request->get('id');
  $up = $request->get('row_id');
  $v = DB::table('product')->where('id', $up)->update([$idx => $request->get('value')]);
  $type = $request->get('type') ?? '';
  if($v){
    $msg = 'Update successfully!';
    if($type=='android'){
      return response()->json(['code' => 0,'response' => trans('true'),'message'=>$msg]);      
    }else{
      \Session::flash('message',$msg);
      return $request->get('value');
    }
  }else{
    $msg = 'Update failed!';
    if($type=='android'){
      return response()->json(['code' => 1,'response' => trans('false'),'message'=>$msg]);     
    }else{
      \Session::flash('message',$msg);
      return $request->get('value');
    }
  }
}
public function saveDecimal(Request $request){
  $this->validate($request, [
    'id' => 'required',
    'row_id' => 'required',
  ]);
  $idx = $request->get('id');
  $up = $request->get('row_id');
  $value = $request->get('value');
  $value = number_format($value, 2, '.', ',');
  $v = DB::table('product')->where('id', $up)->update([$idx => $value]);
  if($v){
    $msg = 'Update successfully!';
      \Session::flash('message',$msg);
      return $value;
  }else{
    $msg = 'Update failed!';
      \Session::flash('message',$msg);
      return $value;
  }
}
public function changeSetting(Request $request){

  $getKey = $request->get('keyname');
  $getVal = $request->get('keyvalue');
  $setting = Setting::where('keyname',$getKey)->first();
  if($setting){
     $setting->keyvalue = $getVal;
     $setting->save();
     return response()->json(['code' => 200 ,'message'=>'Filter changed']);
  }else{
     return response()->json(['code' => 403 ,'message'=>'Error setting']);
  }
}

  public function signuptest(Request $request)
    {
       $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
           
        ]);
 // $this->validate($request, User::$login_validation_rule);
        
          $v = DB::table('admin_login')->where('email', '=', $request->email)->where('role', '=', 'User')->first();
if($v){
    return response()->json(['response' => trans('false'),'message'=>trans('Already Registered Email')]);
}else{

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['password'] = $request->password;
        $data['remark'] = $request->remark;
        $data['role'] = 'User';
        $data['client_code'] = '';
        $data['status'] = '0';
       
        $data2 = 'ziova.xenren@gmail.com';
        
      $emailcontent = array (
          'email' => $request->email
          );
   
      Mail::send(['html'=>'mail'], $emailcontent, function($message) {
         $message->to('ziova.xenren@gmail.com', 'Buy-manairge')->subject
            ('Register confirmation');
         $message->from('ziova.xenren@gmail.com','Buy-manairge');
      });
      
      
      
        $curr=DB::table('admin_login')->insert($data);

      
 return response()->json(['response' => trans('true'),'message'=>trans('Add user successfully')]);
}
           
    }
    
    
     public function savedetails_new(Request $request)
    {
       $this->validate($request, [
            'courier_code' => 'required'
        ]);

        $courier_code = $request->courier_code;
        $scan_time = $request->scan_time;
        $dataCompare = ['courier_code' => $courier_code];
        //$details['client_code'] = $request->client_code;
        $user_id =  $request->user_id;
        $v = User::find($user_id);// DB::table('admin_login')->where('id', '=', $user_id)->first();
        $details['courier_code'] =  $request->courier_code;
        $details['status'] = 'Shipping';
        $details['client_code'] =   $v->client_code;
        $details['scan_time'] = $scan_time ?? '';
         if($v){
          $details1['email'] =  $v->email;
          $details1['phone'] =   $v->phone;
          $details1['courier_code'] =   $courier_code;
          $details1['client_code'] =   $v->client_code;
          $details1['status'] =   'Ordering';
          DB::table('product')->updateOrInsert($dataCompare,$details);
          DB::table('savedetails')->updateOrInsert($dataCompare,$details1);
        return response()->json(['response' => trans('true'),'message'=>trans('Save Successfully')]);   
   //$data=  DB::select("select * from product where courier_code='$courier_code'");
         
//    if(count($data)){
       
//       DB::table('savedetails')->insert($details1);
//       DB::table('product')->where('courier_code', $courier_code)->update($details);
//     return response()->json(['response' => trans('true'),'message'=>trans('Save Successfully')]);
//    }else{
//           DB::table('savedetails')->insert($details1);
    
//      DB::table('product')->insert($details);
//     return response()->json(['response' => trans('true'),'message'=>trans('Save Successfully')]);
// // return response()->json(['response' => trans('false'),'error' => trans('Worng Courier code')]);

//    }
         }else{
              return response()->json(['response' => trans('false'),'error' => trans('Worng user id')]);
         }  

     
    }
    
    public function updatedetails_new(Request $request)
    {
       $this->validate($request, [
            'courier_code' => 'required'
        ]);

        $courier_code = $request->courier_code;
        $details['client_code'] = $request->client_code;
          $user_id =  $request->user_id;
          $details['status'] = 'Arrival';
          $details['scan_time'] = $request->scan_time ?? '';
           $v = DB::table('admin_login')->where('id', '=', $user_id)->first();
          $details1['email'] =  $v->email;
          $details1['phone'] =   $v->phone;
           $details1['courier_code'] =   $request->courier_code;
            $details1['client_code'] =   $request->client_code;
             $details1['status'] =   'Arrival';
  
//$data=  DB::select("select * from product where courier_code='$courier_code'");
$data = Product::withTrashed()->where('courier_code', $courier_code);
$check = $data->first();
if($check){
    DB::table('savedetails')->where('courier_code', $courier_code)->update($details1);
    $data->update($details);
    $msg = $check->trashed() ? 'Courier code : '.$courier_code.' in The Bin' : 'Match Successfully';
    $code = $check->trashed() ? '202' : '0';
    return response()->json(['response' => trans('true'),'code' => $code,'message'=>$msg]);

   }else{

    return response()->json(['response' => trans('false'),'error' => trans('Wrong Courier code')]);
     
   }   
    }

    public function addorupdate(Request $request){
      $this->validate($request, [
        'courier_code' => 'required'
      ]);
      $is_restore = $request->is_restore ?? 'false';
      $is_update = $request->is_update ?? 'false';
      $comp = ['courier_code' => $request->courier_code];
      $up = [
        'courier_code' => $request->courier_code,
        'container_code' => $request->container_code ?? '',
        'size' => $request->size ?? '',
        'weight' => $request->weight ?? ''
      ];
      $data = Product::withTrashed()->where('courier_code', $request->courier_code);
      $check = $data->first();
      $msg = '';
      $code = 0;
      if($check){
        
        if($is_restore=='true'){
          $code = 203;
          $msg = 'Data Restored & Updated successfully';
          $data->update($up);
          $data->restore();
          return response()->json(['response' => trans('true'),'code' => $code,'message'=>$msg]); 
        }
        if($is_update=='true'){
          $code = 204;
          $msg = 'Update record successfully';
          $data->update($up);
          return response()->json(['response' => trans('true'),'code' => $code,'message'=>$msg]);
        }
        if($check->trashed()){
          $code = 202;
          $msg = 'Courier in the bin. Do you want to restore & update it?';
          return response()->json(['response' => trans('true'),'code' => $code,'message'=>$msg]); 
        }
        $code = 201;
        $msg = 'Courier exist! Do you want to update it?';
        return response()->json(['response' => trans('true'),'code' => $code,'message'=>$msg]);
      }else{
        $msg = 'Add record successfully';
        Product::create($up);
        return response()->json(['response' => trans('true'),'code' => $code,'message'=>$msg]);
      }

    }
    
    public function adminlogin(Request $request)
    {

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
           
        ]);
 // $this->validate($request, User::$login_validation_rule);

        
          $v = DB::table('admin_login')->where('email', '=', $request->username)->first();

          print_r($v);die;
if($v){
        if ($request->password==$v->password) {


            
            $curr = \DB::table('admin_login')->where('email',$request->username)->first();
            
          return response()->json(['response' => trans('true'),'data' => $curr]);
        
         
       }else{
          
    return response()->json(['response' => trans('false'),'error' => trans('Invalid Username & Password')]);

       }
   }else{

    return response()->json(['response' => trans('false'),'error' => trans('Invalid Username & Password')]);
   
   }

    }
    
}
