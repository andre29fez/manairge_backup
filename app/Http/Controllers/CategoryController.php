<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {   
        //return 'index cat';
        $data['menu'] = __('menu.news');
        $data['sub_menu'] = 'Category';
        $data['categories'] = Category::latest()->get();
        return view('admin.news.category.index', $data);
    }


    public function create()
    {
        $data['menu'] = __('menu.news');
        $data['sub_menu'] = 'Category';
        return view('admin.news.category.create',$data);
    }


    public function store(Request $request)
    {
        $request->validate([
            'name'   => 'required|unique:categories|max:255',
            // 'image'  => 'required|image|mimes:jpg,png,jpeg'
        ]);

        if ($request->hasFile('image')) {
            $imageName = 'category-'.time().uniqid().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('img/news'), $imageName);
        }else{
            $imageName = 'default-category.png';
        }

        Category::create([
            'name'   => $request->name,
            'slug'   => str_slug($request->name),
            'image'  => $imageName,
            'status' => true
        ]);
        return redirect()->route('category.index')->with(['message' => 'Category created successfully!']);
    }


    public function show(Category $category)
    {
        //
    }

    
    public function edit(Category $category)
    {
        $category = Category::findOrFail($category->id);
        return view('admin.news.category.edit', compact('category'));
    }


    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name'   => 'required|max:255',
            'image'  => 'image|mimes:jpg,png,jpeg'
        ]);
        $status = true;
        $category = Category::findOrFail($category->id);

        if ($request->hasFile('image')) {

            if(file_exists(public_path('img/news/') . $category->image)){
                unlink(public_path('img/news/') . $category->image);
            }
            $imageName = 'category-'.time().uniqid().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('img/news'), $imageName);

        }else{
            $imageName = $category->image;
        }

        $category->update([
            'name'   => $request->name,
            'slug'   => str_slug($request->name),
            'image'  => $imageName,
            'status' => $status
        ]);

        return redirect()->route('category.index')->with(['message' => 'Category updated successfully!']);
    }


    public function destroy(Category $category)
    {
        $category = Category::findOrFail($category->id);

        if(file_exists(public_path('img/news/') . $category->image)){
            unlink(public_path('img/news/') . $category->image);
        }

        $category->delete();

        return back()->with(['message' => 'Category deleted successfully!']);
    }
}
