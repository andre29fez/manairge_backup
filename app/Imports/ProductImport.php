<?php
namespace App\Imports;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Illuminate\Support\Arr;
//Use Maatwebsite\Excel\Concerns\WithStartRow;
use App\User;
use Carbon\Carbon;
use App\Product;
use Auth;
use DB;

class ProductImport implements OnEachRow,WithHeadingRow{
    
// public function startRow(): int
// {
//     return 2;
// }
// public function headingRow(): int
// {
//     return 1;
// }
 public function onRow(Row $row)
    {
            $row      = $row->toArray();
            $v = User::find(Auth::user()->id);
            $dataCompare = [
                'courier_code' => $row['courier_code']
            ];//must
    		$dataInsert = [
    			'container_code' => $row['container_code'] ?? '',
                'Time' => $row['time'] ?? Carbon::now(),
                'discount' => $row['discount'] ?? '',
                'courier_symbol' => $row['courier_symbol'] ?? '',
                'quantity' => $row['quantity'] ?? '',
                'rmb' => $row['rmb'] ?? '',
                'rm' => $row['rm'] ?? '',
                'weight' => $row['weight'] ?? '',
                'size' => $row['size'],//must
                'courier_fee' => $row['courier_fee'] ?? '',
                'amount' => $row['amount'] ?? '',
                'remark' => $row['remark'] ?? '',
                'arrival_date' => $row['arrival_date'] ?? '',
                'client_code' => $row['client_code'] ?? '',
                'status' => $row['status'] ?? 'Shipping'
            ];
            //return dd($dataInsert);
           // $details['status'] = 'Arrival';
            $dbsaveDet = [
                'email' => $v->email,
                'phone' => $v->phone,
                'courier_code' => $row['courier_code'],
                'client_code' => $row['client_code'] ?? '',
                'status' => $row['status'] ?? 'Ordering'
            ];

            if(Product::where('courier_code', $row['courier_code'])->where('item_remark',null)->exists()){
                    $dataInsert = Arr::prepend($dataInsert, $row['item_remark'], 'item_remark');
            }

            if(Product::where('courier_code',$row['courier_code'])->count()==0){
                $dataInsert = Arr::prepend($dataInsert, $row['item_remark'], 'item_remark');
            }
            DB::table('product')->updateOrInsert($dataCompare,$dataInsert);
            DB::table('savedetails')->updateOrInsert($dataCompare,$dbsaveDet);
            return $v;
    }

}


