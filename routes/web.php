<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.index');
})->middleware('guest');

Route::get('/dashboard', 'AdminController@dashboard');
Route::post('/updateprofile/{id}', 'AdminController@updateprofile');
Route::get('/ProfileSetting/{ids?}', 'AdminController@ProfileSetting');
Route::post('/changepassword/{id}', 'AdminController@changepassword');
Route::post('/onchangestatus/{id}', 'AdminController@onchangestatus');
Route::get('/Users', 'AdminController@Users');
Route::post('/useronchangestatus/{id}', 'AdminController@useronchangestatus');
Route::get('/editusers/{id}', 'AdminController@updateusers');
Route::post('/updateuser/{id}', 'AdminController@updateuser');
Route::get('/addusers', 'AdminController@addusers');
Route::post('/adduser', 'AdminController@adduser');
Route::get('/Registerconfirmation/{id}', 'AdminController@Registerconfirmation');
Route::group(['prefix' => 'user'], function() {
    Route::post('/searchdetails', 'AdminController@searchdetails');
    Route::get('/submit', 'AdminController@submitlist');
    Route::get('/submit/{type?}/{db?}', 'AdminController@submitaction')->name('submit.action');
});
Route::get('/user/Activation/{token}', 'HomeController@Activation');
Route::get('/test/{cc}', 'HomeController@test');

Route::group(['prefix' => 'admin', 'middleware' => 'role:admin'], function() {
    Route::resource('news','NewsController');
    Route::resource('category','CategoryController');
    Route::get('ProductList', 'AdminController@ProductList')->name('product.list');
    Route::post('searchproduct', 'AdminController@searchproduct');
    Route::get('SearchProductList', 'AdminController@SearchProductList');
    Route::get('editproduct/{id}', 'AdminController@editproduct');
    Route::post('updateproduct/{id}', 'AdminController@updateproduct');
    Route::get('backup', 'AdminController@backup');
    Route::post('generate', 'AdminController@generateBackup')->name('admin.backup.generate');
    Route::post('process', 'AdminController@processBackupFile')->name('admin.backup.process');
    Route::get('AddCsv', 'AdminController@AddCsv');
    Route::post('uploadcsv', 'AdminController@uploadcsv');
    Route::post('excel/{action}/{type}', 'AdminController@excel')->name('admin.excel');
    Route::get('filter', 'AdminController@getFilter')->name('admin.filter');
    
});

Auth::routes([
    'register' => false, 
    'reset' => false
  ]);
Route::get('lang/{lang}', 'AdminController@changeLang');
//Route::get('/home', 'HomeController@index')->name('home');
