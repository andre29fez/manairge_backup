<?php

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', 'ApiController@index');
Route::post('/datalist', 'ApiController@datalist');//type : [client_code|courier_code]
Route::post('/updatedetails', 'ApiController@updatedetails');
Route::post('/savedetails', 'ApiController@savedetails');
Route::post('/checkscannerdetails', 'ApiController@checkscannerdetails');
Route::post('/login', 'ApiController@login');
Route::post('/signup', 'ApiController@signup');
Route::post('/signuptest', 'ApiController@signuptest');
Route::post('/savedetails_new', 'ApiController@savedetails_new');
Route::post('/updatedetails_new', 'ApiController@updatedetails_new');
Route::post('adminlogin', [ApiController::class , 'adminlogin']);
Route::post('/saveInput',array('as'=> 'api.saveInput', 'uses' => 'ApiController@saveInput'));
Route::post('/saveDec',array('as'=> 'api.saveDec', 'uses' => 'ApiController@saveDecimal'));
Route::post('/action',array('as'=> 'api.action', 'uses' => 'ApiController@actionTable'));
Route::post('/history',array('as'=> 'api.history', 'uses' => 'ApiController@actionHistory'));
Route::post('/addorupdate',array('as'=> 'api.addorupdate', 'uses' => 'ApiController@addorupdate'));
Route::post('/change_setting',array('as'=> 'api.changeSetting', 'uses' => 'ApiController@changeSetting'));
Route::post('/save_clientcode',array('as'=> 'api.saveclient', 'uses' => 'ApiController@saveclient'));
Route::get('/getClient',array('as'=> 'api.getClient', 'uses' => 'ApiController@getClient'));